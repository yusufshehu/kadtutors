<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TutorSubject extends Pivot
{
    public function tutor()
    {
        return $this->belongsTo(Tutor::class, 'tutor_id')->withPivot('is_specialty');
    }
    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }
}
