<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class TutorOffers extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['offererName', 'offerDate'];


    public function tutor(): BelongsTo
    {
        return $this->belongsTo(Tutor::class);
    }

    public function offerer(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getOffererNameAttribute()
    {
        return User::where('id', $this->offerer_user_id)->pluck('name');
        // $specialty = Subject::where('tutor_id', $this->id)->where('is_specialty', 1)->first();
        // return $specialty->name;
    }

    public function getOfferDateAttribute()
    {
        return $this->created_at->diffForHumans();
        // $specialty = Subject::where('tutor_id', $this->id)->where('is_specialty', 1)->first();
        // return $specialty->name;
    }

    public function accept()
    {
        $this->status = 2;
        $this->save();
        // $specialty = Subject::where('tutor_id', $this->id)->where('is_specialty', 1)->first();
        // return $specialty->name;
    }
}
