<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TutorReviews extends Model
{
    use HasFactory;

    public function tutor(): BelongsTo
    {
        return $this->belongsTo(Tutor::class);
    }
}
