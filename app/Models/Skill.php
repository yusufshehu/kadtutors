<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Skill extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function tutor(): BelongsTo
    {
        return $this->belongsTo(Tutor::class);
    }

    // public function tutor(): BelongsToMany
    // {
    //     return $this->belongsToMany(Tutor::class);
    // }

    public function addSkill($tutor_id, $data)
    {
        $this->tutor_id = $tutor_id;
        $this->skill_name = $data['name'];
        $this->save();
    }
}
