<?php

namespace App\Models;

use Exception;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasApiTokens, HasRoles;

    // protected $with = ['userable'];

    protected $appends = ['signedUpOn', 'full_name'];

    public function tutorAccount(): HasOne
    {
        return $this->hasOne(Tutor::class, 'user_id');
    }

    public function clientAccount(): HasOne
    {
        return $this->hasOne(Client::class, 'user_id');
    }

    public function userable(): MorphTo
    {
        return $this->morphTo();
    }


    // protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'middle_name',
        'last_name',
        'phone_number',
        'account_type',
        'userable_type',
        'userable_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }



    public function getSignedUpOnAttribute()
    {
        return $this->created_at->diffForHumans();
        // $specialty = Subject::where('tutor_id', $this->id)->where('is_specialty', 1)->first();
        // return $specialty->name;
    }

    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . ($this->middle_name ? $this->middle_name . ' ' : '') . $this->last_name;
    }

    public function updateProfilePic($data)
    {
        if ($this->profile_picture_url) {
            try {
                $old_image_url = $this->profile_picture_url;
                Storage::disk('s3')->delete($old_image_url);
            } catch (Exception $e) {
                return "Error: " . $e->getMessage();
            }
        }
        $path = $data->file('profile_picture')->store('profile_pictures', 's3');
        $this->profile_picture_url = Storage::disk('s3')->url($path);
        $this->save();
    }
}
