<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TutorJobs extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function tutor(): BelongsTo
    {
        return $this->belongsTo(Tutor::class);
    }

    public function addJob($tutor_id, $data)
    {
        $this->tutor_id = $tutor_id;
        $this->save();
    }
}
