<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Subject extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function tutor(): BelongsTo
    {
        return $this->belongsTo(Tutor::class);
    }

    // public function tutor(): BelongsToMany
    // {
    //     return $this->belongsToMany(Tutor::class);
    // }
    public function addSubject($tutor_id, $data, $isSpecialty = false)
    {
        $this->tutor_id = $tutor_id;
        $this->name = "dlslls";
        $this->is_specialty = $isSpecialty;
        $this->save();
    }

    // public function specialty($tutor_id){

    //     return $t
    // }
}
