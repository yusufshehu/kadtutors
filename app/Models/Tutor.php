<?php

namespace App\Models;

use App\Mail\TutorApplicationApprovalMail;
use App\Mail\TutorApplicationApprovalMailAndPasswordResetNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Tutor extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $appends = ['full_name'];

    // public function subjects(): HasMany
    // {
    //     return $this->hasMany(Subject::class);
    // }

    // public function subjects(): BelongsToMany
    // {
    //     return $this->belongsToMany(Subject::class)->withPivot('is_specialty');
    // }

    public function userAccount(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function jobs(): HasMany
    {
        return $this->hasMany(TutorJobs::class);
    }

    public function reviews(): HasMany
    {
        return $this->hasMany(Subject::class);
    }

    // public function skills(): HasMany
    // {
    //     return $this->hasMany(Skill::class);
    // }

    // public function schedule(): HasMany
    // {
    //     //days that the tutor will be available
    //     return $this->hasMany(TutorSchedule::class);
    // }

    // public function schedule(): BelongsToMany
    // {
    //     //days that the tutor will be available
    //     return $this->belongsToMany(Schedule::class);
    // }

    public function schedules(): HasMany
    {
        return $this->hasMany(TutorSchedule::class, 'tutor_id');
    }

    public function subjects(): HasMany
    {
        return $this->hasMany(TutorSubject::class, 'tutor_id');
    }

    public function skills(): HasMany
    {
        return $this->hasMany(TutorSkill::class, 'tutor_id');
    }

    // public function requests(): HasMany
    // {
    //     //requests made for a tutor
    //     return $this->hasMany(TutorRequests::class);
    // }

    public function offers(): HasMany
    {
        //offers made to a tutor
        return $this->hasMany(Offer::class);
    }

    // public function application(): HasOne
    // {
    //     return $this->hasOne(Application::class);
    // }

    public function application(): HasOne
    {
        return $this->hasOne(TutorApplication::class);
    }



    public function getFullNameAttribute()
    {
        $userAccount = $this->userAccount;
        return $userAccount->first_name . ' ' . $userAccount->middle_name . ' ' . $userAccount->last_name;
        // $specialty = Subject::where('tutor_id', $this->id)->where('is_specialty', 1)->first();
        // return $specialty->name;
    }





    public function newApplication($data)
    {

        // $this->email_address = $data['tutor_details']['email_address'];
        // $this->bio = $data['tutor_details']['bio'];
        // $this->available_days = $data['tutor_details']['available_days'];
        // $this->approved = false;
        $path = $data->file('image')->store('img', 's3');
        $this->profile_picture_url = Storage::disk('s3')->url($path);
        $this->first_name = $data['first_name'];
        $this->middle_name = $data['middle_name'];
        $this->last_name = $data['last_name'];
        $this->email = $data['email'];
        $this->phone_number = $data['phone_number'];
        $this->bio = $data['bio'];
        // $this->state = $data['state'];
        // $this->city = $data['city'];
        // $this->address = $data['address'];
        // $this->days_available = json_encode($data['available_days']);
        // if (!is_null($data->file('image'))) {
        // $this->profile_picture_url = "lfkdkdf";
        // }
        $this->status = 1;
        $this->save();

        TutorApplication::create(
            [
                'tutor_id' => $this->id,
                // 'status' => config('enums.tutor_application.statuses')[0]
                'status' => 1
            ]
        );
        $tutorAvailableDays = explode(",", $data['available_days']);

        for ($i = 0; $i < count($tutorAvailableDays); $i++) {
            # code...
            TutorSchedule::create(
                [
                    'tutor_id' => $this->id,
                    'name' => $tutorAvailableDays[$i]
                ]
            );
        }



        // Application::

        return $this->id;
    }
    public function updateApplicationStatus($status)
    {
        $this->application->status = $status;
        //this will be the status of the tutor
        $this->status = $status;
        $this->application->save();
        $this->save();
    }

    public function createUserAccount()
    {
        $user = User::firstWhere('email', $this->email);
        if (!$user) {
            $randomPassword = Str::random(8);
            # code...
            $newUser = User::create(
                [
                    'email' => $this->email,
                    'password' => Hash::make($randomPassword),
                    'first_name' => $this->first_name,
                    'middle_name' => $this->middle_name,
                    'last_name' => $this->last_name,
                    'phone_number' => $this->phone_number,
                ]
            );

            // $token = $newUser->createToken('auth_token')->plainTextToken;
            // $tokenPassword = substr($newUser->tokens->first()->token, 0, 8);
            // $newUser->password  = $tokenPassword;
            // $newUser->save();
            $newUser->assignRole('Tutor');
            $this->user_id = $newUser->id;
            $this->save();
            Mail::to($newUser->email)->send(
                new TutorApplicationApprovalMailAndPasswordResetNotification
            );
        } else {
            $this->user_id = $user->id;
            $this->save();
            $user->assignRole('Tutor');

            Mail::to($this->email)->send(new TutorApplicationApprovalMail);
        }

        $this->status = 2;
        $this->application->update(['status' => 2]);
        // //this will be the status of the tutor
        // // $this->status = $status;
        $this->save();
    }

    public function updateProfilePicture($file)
    {
        $path = $file->store('img', 's3');
        $this->userAccount->profile_picture_url = Storage::disk('s3')->url($path);
        $this->save();
    }

    public function profileSetup($data)
    {
        $path = $file->store('img', 's3');
        $this->profile_picture_url = Storage::disk('s3')->url($path);
        $this->save();
    }
}
