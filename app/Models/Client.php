<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Client extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function jobPost(): HasMany
    {
        return $this->hasMany(JobPost::class);
    }
    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class);
    }
}
