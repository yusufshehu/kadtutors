<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TutorsJobs extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function addJob($tutor_id, $data)
    {
        $this->tutor_id = $tutor_id;
        $this->save();
    }
}
