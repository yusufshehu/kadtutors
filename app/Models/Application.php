<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

//this is the application a tutor submits when he wants to join as a tutor
class Application extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function tutor(): BelongsTo
    {
        return $this->belongsTo(Tutor::class);
    }
}
