<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class JobPost extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = ['posted_at', 'trimmed_description'];

    public function client(): BelongsTo
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function scopeFilter($query)
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function getPostedAtAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function getTrimmedDescriptionAttribute()
    {
        $shortenedText = substr($this->job_description, 0, 200);
        $shortenedText .= (strlen($this->job_description) > 200) ? '...' : '';
        return strip_tags($shortenedText);
    }
}
