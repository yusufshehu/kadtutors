<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TutorSkill extends Pivot
{
    public function tutor()
    {
        return $this->belongsTo(Tutor::class, 'tutor_id');
    }
    public function skill()
    {
        return $this->belongsTo(Skill::class, 'skill_id');
    }
}
