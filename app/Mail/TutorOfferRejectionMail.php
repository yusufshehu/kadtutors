<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TutorOfferRejectionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $tutorName;
    public $offerInfo;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tutorName, $offerInfo)
    {
        $this->offerInfo = $offerInfo;
        $this->tutorName = $tutorName;

        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tutorName = $this->tutorName;
        $offerInfo = $this->offerInfo;
        return $this->markdown('emails.tutorOfferAcceptanceMail', compact('tutorName', 'offerInfo'));
    }
}
