<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TutorRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    // public $data;
    public $tutorName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tutorName)
    {
        // $this->data = $data;
        $this->tutorName = $tutorName;


        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // dd($this->tutorName);
        $tutorName = $this->tutorName;
        $tutorData[] = $this->tutorName;
        return $this->markdown('emails.tutorRequestMail',  compact('tutorData'));
    }
}
