<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TutorOfferMail extends Mailable
{
    use Queueable, SerializesModels;
    public $tutorName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tutorName)
    {
        // $this->data = $data;
        $this->tutorName = $tutorName;


        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tutorName = $this->tutorName;
        $tutorData[] = $this->tutorName;
        return $this->markdown('emails.tutorOfferMail', compact('tutorData'));
    }
}
