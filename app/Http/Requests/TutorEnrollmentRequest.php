<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TutorEnrollmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $emailUniqueValidationRule = ($this->method() == 'POST' ? 'unique:tutors'
            : 'unique:tutors,email,' . $this->id);
        $uniquePhoneNumberValidationRule = ($this->method() == 'POST' ? 'unique:tutors'
            : 'unique:tutors,phone_number,' . $this->id);
        $validationRules =  [
            //
            'first_name'         => ['required', 'string', 'max:191'],
            'last_name'          => ['required', 'string', 'max:191'],
            'middle_name'        => ['string', 'max:191', 'nullable'],
            'email'              => [
                'required',
                'email',
                $emailUniqueValidationRule,
                'max:191'
            ],
            'image' => ['required'],
            'phone_number'       => [
                'required', 'string',
                $uniquePhoneNumberValidationRule,
                'max:191'
            ],
            'bio'       => ['required', 'string'],
            'available_days'       => ['required', 'string', 'max:191'],
            'specialty.*.name'       => ['required'],
            'specialty.*.language'       => ['required'],
            'jobs'       => ['nullable'],
            'jobs.*.title' => ['required_with:jobs', 'string', 'max:191'],
            'jobs.*.employer' => ['required_with:jobs', 'string', 'max:191'],
            'jobs.*.description' => ['required_with:jobs', 'string', 'max:191'],
            'subjects'       => ['required'],
            'subjects.*.name' => ['required', 'string', 'max:191'],
            'subjects.*.language' => ['required', 'string', 'max:191'],
            'skills'       => ['nullable'],
            'skills.*.text' => ['required_with:skills', 'string', 'max:191'],
            'skills.*.value' => ['required_with:skills', 'string', 'max:191'],


        ];
        // if ($this->method() == 'POST') {
        //     $validationRules['image'] = ['required'];
        // }
        return $validationRules;
    }
}
