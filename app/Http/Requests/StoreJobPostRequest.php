<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreJobPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string'],
            'job_duration' => ['required', 'integer'],
            'proposed_price' => ['required', 'integer'],
            'number_of_students' => ['required', 'integer'],
            'number_of_sessions_a_week' => ['required', 'integer'],
            'minimum_years_of_experience' => ['required', 'string'],
            'session_days' => ['required'],
            'payment_interval' => ['required'],
            'subjects' => ['required'],
        ];
    }
}
