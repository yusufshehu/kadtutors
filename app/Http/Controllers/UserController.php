<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Client;
use App\Models\Tutor;
use App\Models\TutorOffers;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class UserController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth')->except('store');
    }

    public function getloggedInUserDetails()
    {
        $user = Auth::user();
        return response()->json(
            [
                'user_details' => $user,
                'number_of_accepted_offers' => 2,
                'tutor_account' => $user->tutorAccount
            ]
        );
        # code...
    }


    public function store(StoreUserRequest $request)
    {


        $user = User::create($request->all());
        if ($request->account_type == 1) {
            $client =  Client::create(
                [
                    'user_id' => $user->id,
                    'state_of_residence' => 'Kaduna',
                    'lga_of_residence' => $request->lga_of_residence,
                ]
            );
            $user->update([
                'userable_type' => 'App\Models\Client',
                'userable_id'   => $client->id
            ]);
            $user->assignRole('Client');
        }
        if ($request->account_type == 2) {
            $tutor = Tutor::create(
                [
                    'user_id' => $user->id,
                    'state_of_residence' => 'Kaduna',
                    'lga_of_residence' => $request->lga_of_residence,
                ]
            );
            $user->assignRole('Tutor');
            $user->update([
                'userable_type' => 'App\Models\Tutor',
                'userable_id'   => $tutor->id
            ]);
        }
        Auth::login($user);

        if ($request->account_type == 2) {
            // return Inertia::render('Client/Home');
            // return Inertia::render(
            //     'Client/Home',
            //     [
            //         'user' => $user
            //     ]
            // );
            return redirect()->to("/tutor/profile/setup");
        }
        return redirect()->to("/account/profile");
    }

    public function update(UpdateUserRequest $request, User $user)
    {

        $user->update([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone_number,
            // 'first_name' => $request->first_name,

        ]);
        return back();
    }

    public function updateProfilePic(Request $request, User $user)
    {
        $request->validate([
            'profile_picture' => ['nullable', 'max:1024', 'mimes:png,jpg,jpeg']
        ]);
        $user->updateProfilePic($request);
        return back();
    }

    public function states()
    {
        return response()->json(
            [
                'states' => config('states'),

            ]
        );
    }
}
