<?php

namespace App\Http\Controllers;

use App\Http\Resources\TutorResource;
use App\Models\JobPost;
use App\Models\Schedule;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class UserAccountController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // dd($user);
        return  Inertia::render(
            'Account/Profile',
            [
                'user' => $user->first()
            ]
        );
    }

    public function userTutorProfile()
    {
        $tutor = Auth::user()->userable;
        $skills = [];

        if ($tutor->subjects()->exists() == false || $tutor->skills()->exists() == false || $tutor->schedules()->exists() == false) {
            return redirect()->to('tutor/profile/setup');
        }


        foreach (config('enums.skills') as $skill) {
            $skills[] = $skill;
        }
        // $skills = config('enums.skills');


        return  Inertia::render(
            'Account/TutorProfile',
            [
                'tutor' => new TutorResource($tutor),
                'skills' => $skills,
                'week_days' => Schedule::all()
            ]
        );
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function showProfile()
    {
        $user = Auth::user()->load('userable');
        $tutor_subjects = $user->userable->subjects ?? null;
        return  Inertia::render(
            'Account/Profile',
            [
                'user' => $user,
                'tutor_subjects' => $tutor_subjects
            ]
        );
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function settings(User $user)
    {
        Inertia::render(
            'Account/Settings',
            [
                'user' => $user->first()
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function jobPostings(Request $request)
    {
        // dd(Auth::user());
        $drafted_job_postings = JobPost::where('client_id', Auth::user()->clientAccount->id)->where('status', 1)->get();
        $posted_job_postings = JobPost::where('client_id', Auth::user()->clientAccount->id)->where('status', 2)->get();
        return Inertia::render(
            'Account/JobPostings',
            [
                'drafted_job_postings' => $drafted_job_postings,
                'posted_job_postings' => $posted_job_postings
            ]
        );
    }


    public function jobOffers(Request $request)
    {
        // dd(Auth::user());
        // dd(Auth::user()->userable->offers->where('status', 1));
        $user = Auth::user();
        $pending_offers = $user->userable->offers()->where('status', 1)->with('tutor.userAccount')->paginate();
        $accepted_offers = $user->userable->offers()->where('status', 2)->with('tutor.userAccount')->paginate();

        $data =
            [
                'pending_offers' => $pending_offers,
                'accepted_offers' => $accepted_offers
            ];

        // dd($data);

        if ($user->userable_type == "App\Models\Tutor") {
            return Inertia::render(
                'Account/Tutor/JobOffers',
                [
                    'pending_offers' => $pending_offers,
                    'accepted_offers' => $accepted_offers
                ]

            );
        }
        if ($user->userable_type == "App\Models\Client") {
            return Inertia::render(
                'Account/Client/JobOffers',
                [
                    'pending_offers' => $pending_offers,
                    'accepted_offers' => $accepted_offers
                ]
            );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
