<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreJobPostRequest;
use App\Http\Requests\UpdateJobPostRequest;
use App\Models\JobPost;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\JobPostResource;
use Illuminate\Http\Request;
use Inertia\Inertia;

class JobPostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'customIndex']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $job_posts = JobPost::orderBy('created_at', 'desc')->where('status', 2)->paginate(10);
        return Inertia::render(
            'JobPosts/Index',
            [
                'job_posts' => $job_posts
            ]
        );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customIndex()
    {
        return response()->json(
            [
                'jobs' => JobPost::paginate(6)
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreJobPostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJobPostRequest $request)
    {
        $subjects = json_encode($request->subjects);
        $session_days = json_encode($request->session_days);
        $payment_interval = $request->payment_interval['value'];
        $job_description = $request->description;
        // dd(Auth::user()->clientAccount->id);

        JobPost::create(
            $request->except('subjects', 'session_days', 'payment_interval', 'number_of_sessions_a_week', 'description')
                + ['client_id' => Auth::user()->clientAccount->id]
                + ['subjects' => $subjects]
                + ['session_days' => $session_days]
                + ['payment_interval' => $payment_interval]
                + ['job_description' => $job_description]
                + ['number_of_sessions_a_week' => $request->number_of_sessions_a_week]
        );

        return redirect('/job-posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobPost  $jobPost
     * @return \Illuminate\Http\Response
     */
    public function show(JobPost $jobPost)
    {
        $jobPost = new JobPostResource($jobPost);
        // dd($jobPost);
        return Inertia::render(
            'JobPosts/Show',
            [
                'job_post' => $jobPost
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return Inertia::render('Client/JobPosts/Create');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(JobPost $jobPost)
    {
        $subjects = json_decode($jobPost->subjects);
        $jobPost->subjects = $subjects;

        $session_days = json_decode($jobPost->session_days);
        $jobPost->session_days = $session_days;

        return Inertia::render('Client/JobPosts/Edit', [
            'job_post' => $jobPost
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateJobPostRequest  $request
     * @param  \App\Models\JobPost  $jobPost
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateJobPostRequest $request, JobPost $jobPost)
    {

        $subjects = json_encode($request->subjects);
        $session_days = json_encode($request->session_days);
        $payment_interval = $request->payment_interval;
        $job_description = $request->description;
        // dd(Auth::user()->clientAccount->id);

        $jobPost->update(
            $request->except('subjects', 'session_days', 'payment_interval', 'number_of_sessions_a_week', 'description', 'posted_at', 'trimmed_description')
                + ['client_id' => Auth::user()->clientAccount->id]
                + ['subjects' => $subjects]
                + ['session_days' => $session_days]
                + ['payment_interval' => $payment_interval]
                + ['job_description' => $job_description]
                + ['number_of_sessions_a_week' => $request->number_of_sessions_a_week]
        );
        return redirect('/job-posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobPost  $jobPost
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobPost $jobPost)
    {
        $jobPost->delete();
    }

    public function postDraft(JobPost $jobPost)
    {
        $jobPost->update(
            [
                'status' => 2
            ]
        );
    }
}
