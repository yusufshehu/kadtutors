<?php

namespace App\Http\Controllers;

use App\Http\Resources\TutorResource;
use App\Mail\TutorOfferAcceptanceMail;
use App\Models\Offer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\TutorOfferMail;
use App\Models\Tutor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;

class OfferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $tutorId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $offers = auth()->user()->userable->offers()->where('status', $request->status)->paginate();

        // return
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //
        $tutorOffer = Offer::create(
            [
                'tutor_id' => $request->tutor_id,
                'client_id' => auth()->user()->userable_id,
                'status' => 1, //request pending
                // 'agreement_duration' => $request->agreement_duration,
                'proposed_price' => $request->proposed_price,
                // 'starting_date' => $request->starting_date,
                'number_of_students' => $request->number_of_students,
                'number_of_sessions_a_week' => $request->number_of_sessions_per_week,
                // 'time_of_day' => $request->time_of_day,
                'duration_per_session' => $request->duration_per_session,
                'subjects' =>  json_encode($request->subjects),
                'session_days' => json_encode($request->session_days),
                // 'session_days' => $request->session_days,

                //  $this->available_days = $data['tutor_details']['available_days'];
                // 'session_days' => $request->session_days,

            ]
        );


        $tutorName = Tutor::find($request->tutor_id)->full_name;

        Mail::to(auth()->user()->email)->send(new TutorOfferMail($tutorName));

        return redirect('/tutors');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPendingOffers(Request $request)
    {

        //
        // $tutorPendingRequests = Tutor::firstWhere('user_id', $request->user()->id)->with(
        //     [
        //         'requests' => function ($query) {
        //             $query->where('status', 1);
        //         }
        //     ]
        // )->paginate(10);

        $tutorId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['pending_offers' => Offer::where('tutor_id', $tutorId)->where('status', 1)->paginate(20)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getUserOffers(Request $request)
    {

        $userId = Auth::user()->id;

        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['offers' => Offer::where('offerer_user_id', $userId)->where('status', $request->status)->with('tutor')->paginate(10)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAcceptedOffers(Request $request)
    {

        //
        // $tutorPendingRequests = Tutor::firstWhere('user_id', $request->user()->id)->with(
        //     [
        //         'requests' => function ($query) {
        //             $query->where('status', 1);
        //         }
        //     ]
        // )->paginate(10);

        $tutorId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['accepted_offers' => Offer::where('tutor_id', $tutorId)->where('status', 2)->paginate(20)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }

    /**
     * Agreements are accepted offers
     * Returns logged in User's ongoing agreements
     *
     * @return \Illuminate\Http\Response
     */
    public function getOngoingUserAgreements(Request $request)
    {

        //
        // $tutorPendingRequests = Tutor::firstWhere('user_id', $request->user()->id)->with(
        //     [
        //         'requests' => function ($query) {
        //             $query->where('status', 1);
        //         }
        //     ]
        // )->paginate(10);

        $offereId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['accepted_offers' => Offer::where('user_id', $offereId)->where('status', 3)->paginate(20)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Offer  $tutorOffers
     * @return \Illuminate\Http\Response
     */
    public function show(Offer $offer)
    {
        // if (auth()->user->hasRole('User')) {
        //     # code...
        // }
        // //

        $user = Auth::user();


        if ($user->userable_type == "App\Models\Tutor") {
            return Inertia::render(
                'Account/Tutor/JobOffers/Show',
                [
                    'tutor_offer' => Offer::firstWhere('id', $offer->id),
                    'tutor' => new TutorResource(Offer::firstWhere('id', $offer->id)->tutor)
                ]
            );
        }
        if ($user->userable_type == "App\Models\Client") {
            return Inertia::render(
                'Account/Client/JobOffers/Show',
                [
                    'tutor_offer' => Offer::firstWhere('id', $offer->id),
                    'tutor' => new TutorResource(Offer::firstWhere('id', $offer->id)->tutor)
                ]
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Offer  $tutorOffers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offer $offer)
    {
        //
        if ($request->offer_status == 2) {
            # code...
            $offer->accept();
            $offerer = User::firstWhere('id', $offer->offerer_user_id);
            $tutorName = $offer->tutor->full_name;
            Mail::to($offerer->email)->send(new TutorOfferAcceptanceMail($tutorName, $offer));
            return response('Offer Accepted');
        }

        if ($request->offer_status == 3) {
            # code...
            $offer->accept();
            $offerer = User::firstWhere('id', $offer->offerer_user_id);
            $tutorName = $offer->tutor->full_name;
            Mail::to($offerer->email)->send(new TutorOfferAcceptanceMail($tutorName, $offer));
            return response('Offer Accepted');
        }
    }


    public function createPlan(Request $request)
    {
        # code...
        $offer = Offer::where('id', $request->id)->first();
        // dd($offer);
        $httpRequest = Http::withHeaders(
            [
                "Authorization" => "Bearer " . config('services.paystack.secret'),
                "Cache-Control" => "no-cache"
            ]
        )->post(
            'https://api.paystack.co/plan',
            [
                'name' => $offer->tutor->full_name .
                    $offer->offererName .
                    $offer->id,
                'interval' => 'monthly',
                'amount' => $offer->proposed_price,
            ]
        );
        // $httpRequest = Http::post(
        //     'https://api.paystack.co/plan',
        //     [

        //     ]
        // );
        // dd($httpRequest->body());
        $response = json_decode($httpRequest->body());
        return response()->json(
            [
                'plan_code' => $response->data->plan_code,
                'key' => config('services.paystack.key')
                // 'plan_code' => $response->data->plan_code,
            ]
        );
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Offer  $tutorOffers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offer $offer)
    {
        //
    }
}
