<?php

namespace App\Http\Controllers\API;

use App\Models\Tutor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TutorEnrollmentRequest;
use App\Mail\TutorApplicationApprovalMail;
use App\Mail\TutorApplicationMail;
use App\Mail\TutorApplicationRejectionMail;
use App\Models\Schedule;
use App\Models\Skill;
use App\Models\Subject;
use App\Models\TutorJobs;
use App\Models\TutorSchedule;
use App\Models\TutorSkill;
use App\Models\TutorSubject;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TutorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): JsonResponse
    {
        //
        $tutors = Tutor::with(["subjects", "skills"])->where('status', 2)
            ->when(
                $request->day_available,
                function ($query) use ($request) {
                    $query->whereHas(
                        'schedule',
                        function ($query) use ($request) {
                            $query->where('name', $request->day_available);
                        }
                    );
                }
            )
            ->when(
                $request->subject,
                function ($query) use ($request) {
                    $query->whereHas(
                        'subjects',
                        function ($query) use ($request) {
                            $query->where('name', $request->subject);
                        }
                    );
                }
            )
            // ->when(
            //     $request->years_of_experience,
            //     function ($query) use ($request) {
            //         $query->whereHas(
            //             'subjects',
            //             function ($query) use ($request) {
            //                 $query->where('name', $request->day_available);
            //             }
            //         );
            //     }
            // )

            ->paginate(20);

        // return response()->json(["tutors" => $tutors]);
        return response()->json(['tutors' => $tutors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TutorEnrollmentRequest $request)
    {

        //
        $tutor = new Tutor();
        $subject = new Subject();
        $skill = new Skill();
        $tutorJob = new TutorJobs();

        $tutor_id = $tutor->newApplication($request);

        $subjects = json_decode($request->subjects);
        $specialty = json_decode($request->specialty);
        $skills = json_decode($request->skills);
        $jobs = json_decode($request->jobs);



        foreach ($subjects as $subject) {
            # code...
            $isSpecialty = false;
            if ($subject === $specialty) {
                # code...
                $isSpecialty = true;
            }

            Subject::create(
                [
                    'tutor_id' => $tutor_id,
                    'name' => $subject,
                    'is_specialty' =>  $isSpecialty,
                ]
            );

            //     // return response()->json($subject);
        }

        foreach ($skills as $skill) {
            # code...
            Skill::create(
                [
                    'tutor_id' => $tutor_id,
                    'skill_name' => $skill->text,
                ]
            );
        }

        foreach ($jobs as $job) {
            $isCurrentJob = false;
            if (isset($job->is_current_job)) {
                # code...
                $isCurrentJob = true;
            }
            # code...
            TutorJobs::create(
                [
                    'tutor_id' => $tutor_id,
                    'job_title' =>  $job->title,
                    'employer' => $job->employer,
                    'employment_duration' =>  $job->employment_duration,
                    'is_current_job' =>  $isCurrentJob,
                    // 'employed_from' =>  $job->employed_from,
                    // 'employed_to' => $job->employed_to,
                    'job_description' =>  $job->description,
                ]
            );
        }

        Mail::to($tutor->email)->send(new TutorApplicationMail);

        // return response()->json(count($request->subjects));

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function show(Tutor $tutor)
    {
        $tutor = Tutor::query()->with(['subjects', 'skills', 'jobs'])->where('id', $tutor->id)->first();
        return response()->json(
            [
                'tutor' => $tutor
            ]
        );
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = Auth::user();

        $tutor = $user->tutorAccount;
        dd($tutor->subjects->first()->subject->name);
        // $tutor = Tutor::query()->with(['subjects', 'skills', 'jobs'])->where('id', $tutor->id)->first();
        return response()->json(
            [
                'tutor' => $tutor,
                'user_details' => $user,
                'skills' => $tutor->skills,
                'subjects' => $tutor->subjects,
                'jobs' => $tutor->jobs,


            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tutor $tutor)
    {
        //
        // $tutor->updateApplicationStatus($request->status);
        // return $tutor;
        // return response($request, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function updateApplicationStatus(Request $request, Tutor $tutor)
    {
        //
        $tutor->updateApplicationStatus($request->status);
        if ($request->status ===  2) {
            //approved
            # code...
            // $tutor->status = 3;
            // Mail::to($tutor->email)->send(new TutorApplicationApprovalMail);
            $tutor->createUserAccount();
        }

        if ($request->status ===  3) {
            //rejected
            $tutor->application->update(['status' => 3]);
            $tutor->status = 3;

            # code...
            Mail::to($tutor->email)->send(new TutorApplicationRejectionMail);
        }
        // return $tutor;
        return response()->json(
            [
                'message' => 'Status Updated'
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tutor $tutor)
    {
        //
    }

    /**
     * search for a tutor by name
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function searchTutors(Request $request)
    {
        $search = $request->search;
        $query = Tutor::query()->with('subjects');

        $query->where(
            function ($query) use ($search) {
                $query->where('first_name', 'like', '%' . $search . '%')
                    ->OrWhere('middle_name', 'like', '%' . $search . '%')
                    ->OrWhere('last_name', 'like', '%' . $search . '%');
            }
        );

        return response()->json(
            $query->paginate(20)
        );
        //
        // return response($request, 200);
    }

    /**
     * search for a tutor by name
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tutor  $tutor
     * @return \Illuminate\Http\Response
     */
    public function filterTutors(Request $request)
    {
        $search = $request->search;
        $query = Tutor::query();
        if ($request->has('teaches')) {
            # code...
            $query->whereHas(
                'subjects',
                function ($query) use ($request) {
                    $query->where('name', $request->teaches);
                }
            );
        }
        // if ($request->has('teaches')) {
        //     # code...
        //     $query->whereHas(
        //         'subjects',
        //         function ($query) use ($request) {
        //             $query->where('name', $request->teaches);
        //         }
        //     );
        // }

        // $query->where(
        //     function ($query) use ($search) {
        //         $query->where('first_name', 'like', '%' . $search . '%')
        //             ->OrWhere('middle_name', 'like', '%' . $search . '%')
        //             ->OrWhere('last_name', 'like', '%' . $search . '%');
        //     }
        // );

        return response()->json(
            $query->with('subjects')->paginate(10)
        );
        //
        // return response($request, 200);
    }

    public function setupProfile(Request $request)
    {
        $tutor =  Tutor::find($request->user()->tutorAccount->id);

        $tutor->update(
            [
                'bio' => $request->bio,
                'title' => $request->title,

            ]
        );
        $tutor->updateProfilePicture($request->file('image'));

        $subjects = json_decode($request->subjects);
        $specialty = json_decode($request->specialty);
        $skills = json_decode($request->skills);
        $jobs = json_decode($request->jobs);
        $available_days = json_decode($request->available_days);



        foreach ($subjects as $subject) {
            $isSpecialty = false;
            if ($subject === $specialty) {
                $isSpecialty = true;
            }

            TutorSubject::updateOrCreate(
                [
                    'tutor_id' => $tutor->id,
                    'subject_id' => Subject::where('name', $subject)->first()->id,
                    'is_specialty' =>  $isSpecialty,
                ]
            );
        }

        foreach ($skills as $skill) {
            TutorSkill::updateOrCreate(
                [
                    'tutor_id' => $tutor->id,
                    'skill_id' => $skill->value,
                ]
            );
        }

        foreach ($jobs as $job) {
            $isCurrentJob = false;
            if (isset($job->is_current_job)) {
                $isCurrentJob = true;
            }
            TutorJobs::updateOrCreate(
                [
                    'tutor_id' => $tutor->id,
                    'job_title' =>  $job->title,
                    'employer' => $job->employer,
                    'employment_duration' =>  $job->employment_duration,
                    'is_current_job' =>  $isCurrentJob,
                    'job_description' =>  $job->description,
                ]
            );
        }

        for ($i = 0; $i < count($available_days); $i++) {
            # code...
            TutorSchedule::updateOrCreate(
                [
                    'tutor_id' => $this->id,
                    'schedule_id' => Schedule::where('name', $available_days[$i])->first()->id,
                ]
            );
        }
    }
}
