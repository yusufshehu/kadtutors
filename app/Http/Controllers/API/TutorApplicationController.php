<?php

namespace App\Http\Controllers\API;


use App\Models\TutorApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TutorApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customIndex(Request $request)
    {
        //
        return response()->json(
            [
                'applications' =>  TutorApplication::where(
                    'status',
                    $request->status
                )->with('tutor')->paginate(20)
            ],
            200
        );
    }

    /**
     * Display a listing of the most recent resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pendingApplications()
    {
        //
        return response()->json(
            [
                'recent_pending_applications' =>  TutorApplication::where('status', 1)->take(5)
            ],
            200
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TutorApplication  $tutorApplication
     * @return \Illuminate\Http\Response
     */
    public function show(TutorApplication $tutorApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TutorApplication  $tutorApplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TutorApplication $tutorApplication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TutorApplication  $tutorApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(TutorApplication $tutorApplication)
    {
        //
    }
}
