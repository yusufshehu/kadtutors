<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Client;
use App\Models\Tutor;
use App\Models\TutorOffers;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //
    public function getloggedInUserDetails()
    {
        $user = Auth::user();
        return response()->json(
            [
                'user_details' => $user,
                'number_of_accepted_offers' => 2,
                'tutor_account' => $user->tutorAccount
            ]
        );
        # code...
    }


    public function store(StoreUserRequest $request)
    {


        $user = User::create($request->all());
        if ($request->account_type == 1) {
            Client::create(
                [
                    'user_id' => $user->id,
                    'state_of_residence' => 'Kaduna',
                    'lga_of_residence' => $request->lga_of_residence,
                ]
            );
            $user->assignRole('Client');
        }
        if ($request->account_type == 2) {
            Tutor::create(
                [
                    'user_id' => $user->id,
                    'state_of_residence' => 'Kaduna',
                    'lga_of_residence' => $request->lga_of_residence,
                ]
            );
            $user->assignRole('Tutor');
        }
        Auth::login($user);


        return response()->json(
            [
                'message' => "User Created"
            ]
        );
    }

    public function update(UpdateUserRequest $request, User $user)
    {

        $user->update([
            'first_name' => $request->first_name,
            'middle_name' => $request->middle_name,
            'last_name' => $request->last_name,
            'phone_number' => $request->phone_number,
            // 'first_name' => $request->first_name,

        ]);
        return response()->json(
            [
                'message' => "User Updated"
            ]
        );
    }

    public function states()
    {
        return response()->json(
            [
                'states' => config('states'),

            ]
        );
    }
}
