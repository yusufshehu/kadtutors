<?php

namespace App\Http\Controllers\API;


use App\Models\TutorOffers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\TutorOfferAcceptanceMail;
use App\Mail\TutorOfferMail;
use App\Models\Tutor;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class TutorOffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        //
        $tutorOffer = TutorOffers::create(
            [
                'tutor_id' => $request->tutor_id,
                'offerer_user_id' => $request->user()->id,
                'status' => 1, //request pending
                'agreement_duration' => $request->agreement_duration,
                'proposed_price' => $request->proposed_price,
                'starting_date' => $request->starting_date,
                'number_of_students' => $request->number_of_students,
                'number_of_sessions_a_week' => $request->number_of_sessions_per_week,
                'time_of_day' => $request->time_of_day,
                'duration_per_session' => $request->duration_per_session,
                'subjects' =>  json_encode($request->subjects),
                'session_days' => json_encode($request->session_days),
                // 'session_days' => $request->session_days,

                //  $this->available_days = $data['tutor_details']['available_days'];
                // 'session_days' => $request->session_days,

            ]
        );


        $tutorName = Tutor::firstWhere('id', $request->tutor_id)->full_name;
        // $tutorName = $tutorName->first_name . ' ' .  $tutorName->middle_name . ' ' .  $tutorName->last_name;
        // dd($tutorName);


        Mail::to($request->user()->email)->send(new TutorOfferMail($tutorName));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getPendingOffers(Request $request)
    {

        //
        // $tutorPendingRequests = Tutor::firstWhere('user_id', $request->user()->id)->with(
        //     [
        //         'requests' => function ($query) {
        //             $query->where('status', 1);
        //         }
        //     ]
        // )->paginate(10);

        $tutorId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['pending_offers' => TutorOffers::where('tutor_id', $tutorId)->where('status', 1)->paginate(20)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAcceptedOffers(Request $request)
    {

        //
        // $tutorPendingRequests = Tutor::firstWhere('user_id', $request->user()->id)->with(
        //     [
        //         'requests' => function ($query) {
        //             $query->where('status', 1);
        //         }
        //     ]
        // )->paginate(10);

        $tutorId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['accepted_offers' => TutorOffers::where('tutor_id', $tutorId)->where('status', 2)->paginate(20)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }

    /**
     * Agreements are accepted offers
     * Returns logged in User's ongoing agreements
     *
     * @return \Illuminate\Http\Response
     */
    public function getOngoingUserAgreements(Request $request)
    {

        //
        // $tutorPendingRequests = Tutor::firstWhere('user_id', $request->user()->id)->with(
        //     [
        //         'requests' => function ($query) {
        //             $query->where('status', 1);
        //         }
        //     ]
        // )->paginate(10);

        $offereId = Tutor::firstWhere('user_id', $request->user()->id)->id;
        // $tutorPendingRequests = TutorRequests::where('tutor_id', $request->user()->id)->where('status', 1)->with('tutor')->paginate(10);

        return response()->json(
            ['accepted_offers' => TutorOffers::where('user_id', $offereId)->where('status', 3)->paginate(20)],
            200
        );
        // $pendingRequests = TutorRequest::firstWhere('tutor_id', $t)



    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TutorOffers  $tutorOffers
     * @return \Illuminate\Http\Response
     */
    public function show(TutorOffers $tutorOffer)
    {
        // if (auth()->user->hasRole('User')) {
        //     # code...
        // }
        // //
        return response()->json(
            [
                'tutor_offer' => TutorOffers::firstWhere('id', $tutorOffer->id),
                'tutor' => TutorOffers::firstWhere('id', $tutorOffer->id)->tutor
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TutorOffers  $tutorOffers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TutorOffers $tutorOffer)
    {
        //
        if ($request->offer_status == 2) {
            # code...
            $tutorOffer->accept();
            $offerer = User::firstWhere('id', $tutorOffer->offerer_user_id);
            $tutorName = $tutorOffer->tutor->full_name;
            Mail::to($offerer->email)->send(new TutorOfferAcceptanceMail($tutorName, $tutorOffer));
            return response('Offer Accepted');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TutorOffers  $tutorOffers
     * @return \Illuminate\Http\Response
     */
    public function destroy(TutorOffers $tutorOffers)
    {
        //
    }
}
