<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\Tutor;
use App\Models\TutorApplication;
use App\Models\TutorOffers;
use App\Models\TutorRequests;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Return the totals of resources
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function customIndex(Request $request)
    {
        //
        $totalNumberOfTutors = Tutor::count();
        $totalNumberOfApplications = TutorApplication::count();
        $totalNumberOfApprovedApplications = TutorApplication::where('status', 2)->count();
        return response()->json(
            [
                'recent_pending_applications' =>  TutorApplication::where('status', 1)->with('tutor')->get()->take(5),
                'total_number_of_tutors' =>  $totalNumberOfTutors,
                'total_number_of_applications' => $totalNumberOfApplications,
                'total_number_of_approved_applications' => $totalNumberOfApprovedApplications,
            ],
            200
        );
    }

    /**
     * Return the totals of resources
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getTutorDashboardData(Request $request)
    {
        //
        // $tutor = $request->auth()->user()->tutor;
        $tutor = Tutor::find($request->user()->tutorAccount->id);

        $totalNumberOfOffers = $tutor->offers->count();
        return response()->json(
            [
                'tutor' =>  $tutor,
                'total_number_of_offers' =>  $totalNumberOfOffers,
                'recent_pending_offers' =>  Offer::where('tutor_id', $tutor->id)
                    ->where(
                        'status',
                        1
                    )
                    ->with('tutor')
                    ->get()
                    ->take(5),

            ],
            200
        );
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
