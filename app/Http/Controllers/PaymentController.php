<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\TutorOffers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    //
    public function createPlan(Request $request)
    {
        # code...
        $offer = Offer::where('id', $request->id);
        $response = Http::post(
            'https://api.paystack.co/plan',
            [
                'name' => 'Steve',
                'interval' => 'Monthly',
                'amount' => $offer->proposed_price,
            ]
        );
    }
}
