<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TutorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $tutor_subjects = [];
        $available_days = [];
        $skills = [];
        $specialty_subject = $this->subjects->where('is_specialty', true)->first()->subject->name;
        foreach ($this->subjects as $tutor_subject) {
            $tutor_subjects[] = $tutor_subject->subject;
        }
        foreach ($this->schedules as $tutor_schedule) {
            $available_days[] = $tutor_schedule->schedule;
        }
        foreach ($this->skills as $tutor_skill) {
            $skills[] = $tutor_skill->skill;
        }
        return [
            'created_at' => $this->created_at,
            'id' => $this->id,
            'title' => $this->title,
            'full_name' => $this->full_name,
            'subjects' => $tutor_subjects,
            'available_days' => $available_days,
            'bio' => $this->bio,
            'skills' => $skills,
            'jobs' => $this->jobs,
            'profile_picture_url' => $this->userAccount->profile_picture_url ?? asset('images/blank-profile-picture.svg'),
            'specialty_subject' => $specialty_subject,
            'lga_of_residence' => $this->lga_of_residence,
            'state_of_residence' => $this->state_of_residence,
        ];
    }
}
