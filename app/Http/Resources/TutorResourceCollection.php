<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TutorResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            'data' => $this->collection->map(
                function ($tutor) {
                    $tutor_subjects_names = [];
                    $available_days = [];
                    $tutor['specialty_subject'] = $tutor->subjects->where('is_specialty', true)->first()->subject->name;
                    foreach ($tutor->subjects as $tutor_subject) {
                        $tutor_subjects_names[] = $tutor_subject->subject->name;
                    }
                    foreach ($tutor->schedules as $tutor_schedule) {
                        $available_days[] = $tutor_schedule->schedule->name;
                    }
                    $tutor['subject_names'] = $tutor_subjects_names;
                    $tutor['available_days'] = $available_days;
                    return $tutor;
                }
            ),
            'total' => $this->total(),
            'count' => $this->count(),
            'last_page' => $this->lastPage(),
            'per_page' => $this->perPage(),
            'current_page' => $this->currentPage(),
            'total_pages' => $this->lastPage(),
            'next_page_url' => $this->nextPageUrl(),
            'prev_page_url' => $this->previousPageUrl(),
        ];
    }
}
