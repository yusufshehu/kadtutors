<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobPostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $shortenedText = substr($this->job_description, 0, 200);
        $shortenedText .= (strlen($this->job_description) > 200) ? '...' : '';
        return [
            'created_at' => $this->created_at,
            'id' => $this->id,
            'title' => $this->title,
            'job_description' => $this->job_description,
            'minimum_years_of_experience' => $this->minimum_years_of_experience,
            'number_of_students' => $this->number_of_students,
            'payment_interval' => config('enums.payment_intervals')[$this->payment_interval],
            'posted_at' => $this->posted_at,
            'proposed_price' => $this->proposed_price,
            'session_days' => json_decode($this->session_days),
            'subjects' => json_decode($this->subjects),
            'how_to_apply' => $this->how_to_apply,
            // 'trimmed_description' => $shortenedText
        ];
    }
}
