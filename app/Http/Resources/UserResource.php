<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'full_name' => $this->full_name,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'account_type' => $this->account_type, // Set the account_type to 2
            'roles' => $this->roles,
            'signedUpOn' => '2 months ago', // You may need to calculate this based on created_at
            'userable' => [
                'userable_id' => $this->userable_id,
                'userable_type' => $this->userable_type,
            ],
        ];
    }
}
