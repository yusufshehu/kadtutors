<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\API\AnalyticsController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\DashboardController;
use App\Http\Controllers\API\OfferController;
use App\Http\Controllers\API\TutorApplicationController;
use App\Http\Controllers\API\TutorOffersController;
use App\Http\Controllers\API\TutorRequestsController;
use App\Http\Controllers\API\TutorsController;
use App\Http\Controllers\API\UserController;
use App\Models\TutorRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Broadcast::routes(['middleware' => ['auth:sanctum']]);

// Route::middleware('auth:sanctum')->get(
//     '/user',
//     function (Request $request) {
//         // return $request->user();
//         return response()->json(
//             [
//                 'user' => $request->user(),
//                 'roles' => $request->user()->getRoleNames()
//             ]
//         );
//     }
// );

// Route::resource('users', UserController::class);
// Route::get('states', [UserController::class, 'states']);

// Route::post('register', [AuthController::class, 'create']);
// Route::post('login', [AuthController::class, 'login']);
// Route::post('offer/create/plan', [OfferController::class, 'createPlan']);
// Route::post(
//     'logout',
//     function (Request $request) {
//         $user = request()->user();
//         $user->tokens()->delete();
//         Auth::guard('web')->logout();

//         return response()->json(['done' => true]);
//     }
// )->middleware('auth:sanctum');
// // Route::group(
// //     ['namespace' => 'API'],filterTutors
// // function () {
// Route::resource('tutors', TutorsController::class);
// Route::post('tutors/profile/setup', [TutorsController::class, 'setupProfile']);
// Route::get('tutors/profile/edit', [TutorsController::class, 'edit']);


// Route::get('search-tutors', [TutorsController::class, 'searchTutors']);
// Route::get('filter-tutors', [TutorsController::class, 'filterTutors']);

// Route::patch('tutors/{tutor}/update-application-status', [TutorsController::class, 'updateApplicationStatus']);


// Route::get('get-dashboard-home-data', [DashboardController::class, 'customIndex'])->middleware('auth:sanctum');
// Route::get('get-tutor-dashboard-home-data', [DashboardController::class, 'getTutorDashboardData'])->middleware('auth:sanctum');
// Route::get('get-applications', [TutorApplicationController::class, 'customIndex'])->middleware('auth:sanctum');

// // Route::resource('tutor-requests', TutorRequestsController::class);

// // Route::resource('tutor-offers', TutorOffersController::class)->middleware('auth:sanctum');
// Route::resource('offer', OfferController::class)->middleware('auth:sanctum');


// // Route::get('tutor-requests/{tutor_request}', [TutorRequestsController::class, 'show']);

// Route::get('get/logged-in-user-details', [UserController::class, 'getloggedInUserDetails'])->middleware('auth:sanctum');


// Route::get('get-pending-offers', [OfferController::class, 'getPendingOffers'])->middleware('auth:sanctum');
// Route::get('get-accepted-offers', [OfferController::class, 'getAcceptedOffers'])->middleware('auth:sanctum');
// Route::get('get-user-offers', [OfferController::class, 'getUserOffers'])->middleware('auth:sanctum');
// Route::get('get-ongoing-user-agreements', [OfferController::class, 'getOngoingUserAgreements'])->middleware('auth:sanctum');





//     }
// );
