<?php

use App\Http\Controllers\OfferController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\JobPostController;
use App\Http\Controllers\TutorsController;
use App\Http\Controllers\UserAccountController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get(
    '/',
    function () {
        // sleep(2);
        return Inertia::render('Welcome');
    }
);
Route::get(
    '/about-us',
    function () {
        // sleep(2);
        return Inertia::render('About');
    }
);



// Route::any('{all}', function () {
//     return view('welcome');
// })
//     ->where(['all' => '.*']);
Auth::routes();

Route::get('/sign-up', [RegisterController::class, 'showRegistrationForm']);
Route::resource('users', UserController::class);
Route::resource('offers', OfferController::class);
Route::post('users/{user}/update-profile-pic', [UserController::class, 'updateProfilePic']);

Route::resource('job-posts', JobPostController::class);
Route::resource('userAccounts', UserAccountController::class);
Route::get('account/setting/{user}', [UserAccountController::class, 'settings']);
Route::get('account/job-postings', [UserAccountController::class, 'jobPostings']);
Route::get('account/job-offers', [UserAccountController::class, 'jobOffers']);
Route::get('userAccounts/{user}', [UserAccountController::class, 'show']);
Route::get('account/profile', [UserAccountController::class, 'showProfile']);

Route::get('account/tutor-profile', [UserAccountController::class, 'userTutorProfile']);


Route::resource('tutors', TutorsController::class);

Route::get('tutor/profile/setup', [TutorsController::class, 'showProfileSetupForm']);
Route::post('tutors/profile/setup', [TutorsController::class, 'setupProfile']);


Route::get('tutor/dashboard', [DashboardController::class, 'getTutorDashboardData']);
Route::get('tutors/show/request-form/{tutor}', [TutorsController::class, 'showRequestForm']);
Route::patch('tutors/tutor/update-subjects', [TutorsController::class, 'updateSubjects']);
Route::patch('tutors/tutor/update-bio', [TutorsController::class, 'updateBio']);
Route::patch('tutors/tutor/update-skills', [TutorsController::class, 'updateSkills']);
Route::patch('tutors/tutor/update-job-experience', [TutorsController::class, 'updateJobExpereince']);

Route::patch('job-posts/{job_post}/post-draft', [JobPostController::class, 'postDraft']);
