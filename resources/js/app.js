import './bootstrap';



import Vuelidate from 'vuelidate'
import { InertiaProgress } from '@inertiajs/progress'
import Layout from './Shared/Layout'

import InputField from './Pages/Auth/FormElements/InputField.vue'

window.Vue = require('vue').default;


import Vue from 'vue'
import { createInertiaApp, Head, Link } from '@inertiajs/inertia-vue'

Vue.use(Vuelidate);
Vue.component('input-field', InputField)
createInertiaApp({
     resolve: async name => {
        let page = (await import(`./Pages/${name}`)).default

        //check the page, see if it already has a layout, if not then assign Layout as the layout
        page.layout ??= Layout

        return page;
    },
//   resolve: name => require(`./Pages/${name}`),
  setup({ el, App, props, plugin }) {
      Vue.use(plugin)
    .component('Link', Link)
          .component('Head', Head)

    new Vue({
        render: h => h(App, props),
    }).$mount(el)
    },
  title : title => "Contactutors - " + title
})

InertiaProgress.init({ color: '#FBC597' });
