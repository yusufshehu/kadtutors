export default {
    authCheck() {
        return new Promise((resolve, reject) => {
            // eslint-disable-next-line no-undef
            axios.get('/api/user').then(response => {
                resolve(response);
            }).catch(error => {
                reject(error);
            })
        });
    }
}
