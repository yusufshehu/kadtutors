export default {
    getSubjects() {
        return [
            "Maths",
            "English",
            "Basic Science",
            "Technical Drawing",
            "Literature in English",
            "Phonics",
            "Physics",
            "Chemistry",
            "Further Mathematics",
            "Economics",
            "French",
            "Yoruba",
            "PHE",
            "Numeracy",
            "Fine Arts"
        ]
    },

    getSubjectsObject() {

        const subjects = [
    "Maths",
    "English",
    "Basic Science",
    "Technical Drawing",
    "Literature in English",
    "Phonics",
    "Physics",
    "Chemistry",
    "Further Mathematics",
    "Economics",
    "French",
    "Yoruba",
    "PHE",
    "Numeracy",
    "Fine Arts"
];

        return subjects.map((subject, index) => ({
            name: subject,
            id: index + 1
        }));
    }
}
