@component('mail::message')
# Your Offer Was Accepted!

{{$tutorName}} has accepted your offer.

 <p>
              <span class="font-semibold">To tutor:</span>
              {{ $offerInfo['number_of_students']}} students
            </p>
            {{-- <p>
              <span class="font-semibold">subjects:</span>
              <span v-for="subject in JSON.parse(offer.subjects)" :key="subject">
                {{ subject }},
              </span>
            </p> --}}
 <p>
              <span class="font-semibold"> Number Of Sessions Per Week:</span>
              {{ $offerInfo['number_of_sessions_a_week'] }}
            </p>
             {{-- <p>
              <span class="font-semibold"> Days For Sessions:</span>
              <span v-for="day in json_decode(offer.session_days)" :key="day">
                {{ day }},
              </span>
            </p> --}}

<p>
              <span class="font-semibold"> Time of sessions:</span>
              {{ $offerInfo['time_of_day'] }}
            </p>

<p>
              <span class="font-semibold"> Duration Per Session:</span>
              {{ $offerInfo['duration_per_session'] }}
            </p>

<p>
              <span class="font-semibold">Agreement Duration:</span>
              {{$offerInfo['agreement_duration'] }} Months
            </p>

<p>
              <span class="font-semibold">Starting Date:</span>
              {{ $offerInfo['starting_date'] }}
            </p>

            {{-- <div style="margin-top: 2rem">
              <span class="font-normal">Price</span> --}}
              {{-- <br /> --}}
# ₦{{ $offerInfo['proposed_price'] }}
              {{-- <span class="text-4xl"> ₦{{ $offerInfo['proposed_price'] }} </span> --}}
            {{-- </div> --}}


{{-- @for ($i < count($offerInfo))
{{$offerInfo[$i]}}
@endfor --}}

@component('mail::button', ['url' => env('APP_URL'). '#/offer/make-payment/' . $offerInfo['id']])
Review Offer
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent
