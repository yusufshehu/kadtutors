@component('mail::message')
# Application Received

Your application has been received and is being reviewed, we will contact you for any further updates.

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
