@component('mail::message')
# Request Submitted

Your request for {{$tutorData[0]}} has been submitted.

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
