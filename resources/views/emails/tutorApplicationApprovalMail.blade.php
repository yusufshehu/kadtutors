@component('mail::message')
    # Application Approved!!

    Congratulations your application has been approved. You are now a tutor on Contactutors.
    Click the button below to login and access your tutor dashboard

    <br>


    @component('mail::button', ['url' => config('app.url')])
        Login
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
