@component('mail::message')
    # Application Approved!!

    Congratulations your application has been approved. You are now a tutor on Contactutors.
    Click the button below then type in your email to reset your password and login

    @component('mail::button', ['url' => config('app.url') . '/password/reset'])
        Reset Password
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
