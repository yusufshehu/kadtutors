<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
            @if (trim($slot) === 'Contactutors')
                {{-- <img src="https://laravel.com/img/notification-logo.png" class="logo" alt="Laravel Logo"> --}}
                <img src="{{ asset('images/glasses.svg') }}" class="logo" alt="Contactutors Logo" width="80"
                    height="10">
            @else
                {{ $slot }}
            @endif
        </a>
    </td>
</tr>
