"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Account_JobPosts_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/JobPosts.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/JobPosts.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  methods: {
    switchTab: function switchTab(indexToSwitchTo) {
      this.active_tab_index = indexToSwitchTo;
    },
    getAcceptedOffers: function getAcceptedOffers() {
      var _this = this;

      axios.get("api/get-user-offers?status=2").then(function (response) {
        _this.accepted_offers = response.data.offers.data;
      });
    },
    getPendingOffers: function getPendingOffers() {
      var _this2 = this;

      axios.get("api/get-user-offers?status=1").then(function (response) {
        _this2.pending_offers = response.data.offers.data;
      });
    },
    getOngoingOffers: function getOngoingOffers() {
      var _this3 = this;

      axios.get("api/get-user-offers?status=5").then(function (response) {
        _this3.ongoing_offers = response.data.offers.data;
      });
    }
  },
  data: function data() {
    return {
      user: {},
      active_tab_index: 0,
      tabs: [{
        title: "Ongoing Offers",
        index: 0
      }, {
        title: "Accepted Offers",
        index: 1
      }, {
        title: "Pending Offers",
        index: 2
      }],
      accepted_offers: {},
      pending_offers: {},
      ongoing_offers: {}
    };
  },
  created: function created() {
    this.getAcceptedOffers();
    this.getPendingOffers();
    this.getOngoingOffers();
  }
});

/***/ }),

/***/ "./resources/js/Pages/Account/JobPosts.vue":
/*!*************************************************!*\
  !*** ./resources/js/Pages/Account/JobPosts.vue ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _JobPosts_vue_vue_type_template_id_bf50d4fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./JobPosts.vue?vue&type=template&id=bf50d4fe& */ "./resources/js/Pages/Account/JobPosts.vue?vue&type=template&id=bf50d4fe&");
/* harmony import */ var _JobPosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./JobPosts.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Account/JobPosts.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _JobPosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _JobPosts_vue_vue_type_template_id_bf50d4fe___WEBPACK_IMPORTED_MODULE_0__.render,
  _JobPosts_vue_vue_type_template_id_bf50d4fe___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Account/JobPosts.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Account/JobPosts.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Account/JobPosts.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_JobPosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./JobPosts.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/JobPosts.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_JobPosts_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Account/JobPosts.vue?vue&type=template&id=bf50d4fe&":
/*!********************************************************************************!*\
  !*** ./resources/js/Pages/Account/JobPosts.vue?vue&type=template&id=bf50d4fe& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_JobPosts_vue_vue_type_template_id_bf50d4fe___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_JobPosts_vue_vue_type_template_id_bf50d4fe___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_JobPosts_vue_vue_type_template_id_bf50d4fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./JobPosts.vue?vue&type=template&id=bf50d4fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/JobPosts.vue?vue&type=template&id=bf50d4fe&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/JobPosts.vue?vue&type=template&id=bf50d4fe&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/JobPosts.vue?vue&type=template&id=bf50d4fe& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container" }, [
    _c(
      "div",
      {
        staticClass:
          "flex w-full place-items-center place-content-center justify-center flex-row",
      },
      [
        _c(
          "div",
          { staticClass: "tabs justify-center" },
          _vm._l(_vm.tabs, function (current_tab, index) {
            return _c(
              "a",
              {
                key: index,
                staticClass: "tab tab-bordered text-black",
                class: {
                  "tab-active": current_tab.index === _vm.active_tab_index,
                },
                on: {
                  click: function ($event) {
                    return _vm.switchTab(current_tab.index)
                  },
                },
              },
              [_vm._v(_vm._s(current_tab.title))]
            )
          }),
          0
        ),
      ]
    ),
    _vm._v(" "),
    _c("div", [
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.active_tab_index == 0,
              expression: "active_tab_index == 0",
            },
          ],
          staticClass: "sm:flex flex-col md:grid grid-cols-3 md:gap-4",
        },
        [
          _vm.ongoing_offers.length == 0
            ? _c("div", { staticClass: "mt-5" }, [
                _c("p", [
                  _vm._v("Seems like you haven't made any offers yet."),
                ]),
                _vm._v(" "),
                _c(
                  "button",
                  {
                    staticClass: "btn mt-5",
                    on: {
                      click: function ($event) {
                        {
                          _vm.$router.push({
                            name: "Tutors",
                          })
                        }
                      },
                    },
                  },
                  [_vm._v("\n          Find a tutor\n        ")]
                ),
              ])
            : _vm._e(),
          _vm._v(" "),
          _vm._l(_vm.ongoing_offers, function (ongoing_offer) {
            return _c(
              "div",
              {
                key: ongoing_offer.id,
                staticClass: "card bg-base-100 shadow-xl mb-5",
              },
              [
                _c("div", { staticClass: "card-body" }, [
                  _c("h2", { staticClass: "card-title" }, [
                    _c("div", { staticClass: "avatar" }, [
                      _c("div", { staticClass: "w-24 rounded" }, [
                        _c("img", {
                          attrs: { src: ongoing_offer.tutor.profile_pic_url },
                        }),
                      ]),
                    ]),
                  ]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v("If a dog chews shoes whose shoes does he choose?"),
                  ]),
                  _vm._v(" "),
                  _vm._m(0, true),
                ]),
              ]
            )
          }),
        ],
        2
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.active_tab_index == 1,
              expression: "active_tab_index == 1",
            },
          ],
          staticClass: "sm:flex flex-col md:grid grid-cols-3 md:gap-4",
        },
        _vm._l(_vm.accepted_offers, function (accepted_offer) {
          return _c(
            "div",
            {
              key: accepted_offer.id,
              staticClass: "card bg-base-100 shadow-xl mb-5",
            },
            [
              _c("div", { staticClass: "card-body" }, [
                _c("h2", { staticClass: "card-title" }, [
                  _c("div", { staticClass: "avatar" }, [
                    _c("div", { staticClass: "w-24 rounded" }, [
                      _c("img", {
                        attrs: { src: accepted_offer.tutor.profile_pic_url },
                      }),
                    ]),
                  ]),
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v("If a dog chews shoes whose shoes does he choose?"),
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-actions justify-end" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      on: {
                        click: function ($event) {
                          return _vm.$router.push({
                            name: "Accepted Offer Payment",
                            params: { offer_id: accepted_offer.id },
                          })
                        },
                      },
                    },
                    [_vm._v("\n              Make Payment\n            ")]
                  ),
                ]),
              ]),
            ]
          )
        }),
        0
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.active_tab_index == 2,
              expression: "active_tab_index == 2",
            },
          ],
          staticClass: "sm:flex flex-col md:grid grid-cols-3 md:gap-4",
        },
        _vm._l(_vm.pending_offers, function (pending_offer) {
          return _c(
            "div",
            {
              key: pending_offer.id,
              staticClass: "card bg-base-100 shadow-xl mb-5",
            },
            [
              _c("div", { staticClass: "card-body" }, [
                _c("h2", { staticClass: "card-title" }, [
                  _c("div", { staticClass: "avatar" }, [
                    _c("div", { staticClass: "w-24 rounded" }, [
                      _c("img", {
                        attrs: { src: pending_offer.tutor.profile_pic_url },
                      }),
                    ]),
                  ]),
                ]),
                _vm._v(" "),
                _c("p", [
                  _vm._v("If a dog chews shoes whose shoes does he choose?"),
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "card-actions justify-end" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      on: {
                        click: function ($event) {
                          return _vm.$router.push({
                            name: "Offer Details",
                            params: { offer_id: pending_offer.id },
                          })
                        },
                      },
                    },
                    [_vm._v("\n              Details\n            ")]
                  ),
                ]),
              ]),
            ]
          )
        }),
        0
      ),
    ]),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-actions justify-end" }, [
      _c("button", { staticClass: "btn btn-primary" }, [_vm._v("Details")]),
    ])
  },
]
render._withStripped = true



/***/ })

}]);