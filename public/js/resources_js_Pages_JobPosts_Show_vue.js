"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_JobPosts_Show_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/JobPosts/Show.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/JobPosts/Show.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-vue */ "./node_modules/@inertiajs/inertia-vue/dist/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Head: _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_0__.Head
  },
  props: {
    job_post: {
      type: Object
    }
  },
  methods: {
    copyToClipboard: function copyToClipboard() {
      // Create a textarea element to hold the text to be copied
      var textArea = document.createElement("textarea");
      textArea.value = window.location.href; // Replace with the text you want to copy
      // Append the textarea to the document

      document.body.appendChild(textArea); // Select the text in the textarea

      textArea.select(); // Copy the selected text to the clipboard

      document.execCommand("copy"); // Remove the textarea from the document

      document.body.removeChild(textArea); // Optionally, you can show a success message to the user

      alert("Link copied to clipboard!");
    }
  }
});

/***/ }),

/***/ "./resources/js/Pages/JobPosts/Show.vue":
/*!**********************************************!*\
  !*** ./resources/js/Pages/JobPosts/Show.vue ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Show_vue_vue_type_template_id_69ec159d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=69ec159d& */ "./resources/js/Pages/JobPosts/Show.vue?vue&type=template&id=69ec159d&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/Pages/JobPosts/Show.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_69ec159d___WEBPACK_IMPORTED_MODULE_0__.render,
  _Show_vue_vue_type_template_id_69ec159d___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/JobPosts/Show.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/JobPosts/Show.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/JobPosts/Show.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/JobPosts/Show.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/JobPosts/Show.vue?vue&type=template&id=69ec159d&":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/JobPosts/Show.vue?vue&type=template&id=69ec159d& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_69ec159d___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_69ec159d___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_69ec159d___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Show.vue?vue&type=template&id=69ec159d& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/JobPosts/Show.vue?vue&type=template&id=69ec159d&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/JobPosts/Show.vue?vue&type=template&id=69ec159d&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/JobPosts/Show.vue?vue&type=template&id=69ec159d& ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "text-black p-5 md:p-12 flex flex-col space-y-10" },
    [
      _c("div", { staticClass: "flex flex-row space-x-4 py-5" }, [
        _c("h1", { staticClass: "text-4xl font-bold" }, [
          _vm._v("\n      " + _vm._s(_vm.job_post.data.title) + "\n      "),
          _c("br"),
          _vm._v(" "),
          _c("span", { staticClass: "text-sm font-light" }, [
            _vm._v(
              "\n        Posted " +
                _vm._s(_vm.job_post.data.posted_at) +
                "\n      "
            ),
          ]),
        ]),
        _vm._v(" "),
        _c("div", [
          _c("a", { staticClass: "mt-5", on: { click: _vm.copyToClipboard } }, [
            _c(
              "svg",
              {
                staticClass: "w-6 h-6",
                attrs: {
                  xmlns: "http://www.w3.org/2000/svg",
                  fill: "none",
                  viewBox: "0 0 24 24",
                  "stroke-width": "1.5",
                  stroke: "currentColor",
                },
              },
              [
                _c("path", {
                  attrs: {
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    d: "M13.19 8.688a4.5 4.5 0 011.242 7.244l-4.5 4.5a4.5 4.5 0 01-6.364-6.364l1.757-1.757m13.35-.622l1.757-1.757a4.5 4.5 0 00-6.364-6.364l-4.5 4.5a4.5 4.5 0 001.242 7.244",
                  },
                }),
              ]
            ),
          ]),
        ]),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "flex flex-row flex-wrap" }, [
        _c("div", { staticClass: "pr-5 md:w-1/3" }, [
          _c(
            "div",
            {
              staticClass:
                "flex flex-col space-y-5 border p-5 h-fit rounded-md border-primary",
            },
            [
              _c("div", [
                _c("h4", { staticClass: "font-medium mb-2" }, [
                  _vm._v("Subjects"),
                ]),
                _vm._v(" "),
                _c(
                  "ul",
                  { staticClass: "font-light" },
                  _vm._l(_vm.job_post.data.subjects, function (subject, index) {
                    return _c("li", { key: index }, [
                      _vm._v(
                        "\n              " + _vm._s(subject) + "\n            "
                      ),
                    ])
                  }),
                  0
                ),
              ]),
              _vm._v(" "),
              _c("div", [
                _c("h4", { staticClass: "font-medium mb-2" }, [
                  _vm._v("Sessions"),
                ]),
                _vm._v(" "),
                _c(
                  "ul",
                  { staticClass: "font-light" },
                  _vm._l(_vm.job_post.data.session_days, function (day, index) {
                    return _c("li", { key: index }, [
                      _vm._v(
                        "\n              " + _vm._s(day) + "\n            "
                      ),
                    ])
                  }),
                  0
                ),
              ]),
              _vm._v(" "),
              _c("div", [
                _c("h4", { staticClass: "font-medium mb-2" }, [
                  _vm._v(
                    "\n            Number Of Students : " +
                      _vm._s(_vm.job_post.data.number_of_students) +
                      "\n          "
                  ),
                ]),
                _vm._v(" "),
                _c("h4", { staticClass: "mb-2" }, [
                  _c("span", { staticClass: "font-medium" }, [
                    _vm._v(" Proposed Salary "),
                  ]),
                  _vm._v(
                    " : ₦" +
                      _vm._s(_vm.job_post.data.proposed_price) +
                      "\n            " +
                      _vm._s(_vm.job_post.data.payment_interval) +
                      "\n          "
                  ),
                ]),
                _vm._v(" "),
                _c("h4", { staticClass: "mb-2" }, [
                  _c("span", { staticClass: "font-medium" }, [
                    _vm._v(
                      "\n              Minimum Years Of Teaching Experience\n            "
                    ),
                  ]),
                  _vm._v(
                    "\n            : " +
                      _vm._s(_vm.job_post.data.minimum_years_of_experience) +
                      "\n          "
                  ),
                ]),
              ]),
            ]
          ),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "md:w-2/3 flex flex-col space-y-7" }, [
          _c("div", [
            _c("h2", { staticClass: "text-2xl font-medium mb-3" }, [
              _vm._v("Job Description"),
            ]),
            _vm._v(" "),
            _c("p", {
              domProps: {
                innerHTML: _vm._s(_vm.job_post.data.job_description),
              },
            }),
          ]),
          _vm._v(" "),
          _c("div", [
            _c("h2", { staticClass: "text-2xl font-medium mb-3" }, [
              _vm._v("How To Apply"),
            ]),
            _vm._v(" "),
            _c("p", {
              domProps: { innerHTML: _vm._s(_vm.job_post.data.how_to_apply) },
            }),
          ]),
        ]),
      ]),
      _vm._v(" "),
      _c("Head", { attrs: { title: "Job Post Details" } }),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);