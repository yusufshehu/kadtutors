"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Tutors_Show_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Components/Modal.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Components/Modal.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: ["show_modal"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Tutors/Show.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Tutors/Show.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-vue */ "./node_modules/@inertiajs/inertia-vue/dist/index.js");
/* harmony import */ var _Components_Modal_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Components/Modal.vue */ "./resources/js/Pages/Components/Modal.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Head: _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_0__.Head,
    Modal: _Components_Modal_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  props: {
    tutor: {
      type: Object
    }
  },
  data: function data() {
    return {
      //   tutor: {},
      show_job_details_modal: false,
      is_logged_in: false,
      job_details_to_show: {}
    };
  },
  name: "TutorProfile",
  methods: {
    hireTutor: function hireTutor() {
      if (this.is_logged_in) {
        this.$router.push({
          name: "Request Tutor",
          params: {
            tutor_id: "".concat(this.$route.params.tutor_id)
          }
        });
      } else {
        this.$router.push({
          name: "Login" // params: {
          //     tutor_id: `${tutor.id}`
          // },

        });
      }
    }
  }
});

/***/ }),

/***/ "./resources/js/Pages/Components/Modal.vue":
/*!*************************************************!*\
  !*** ./resources/js/Pages/Components/Modal.vue ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Modal_vue_vue_type_template_id_33c605fd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Modal.vue?vue&type=template&id=33c605fd& */ "./resources/js/Pages/Components/Modal.vue?vue&type=template&id=33c605fd&");
/* harmony import */ var _Modal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Modal.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Components/Modal.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Modal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Modal_vue_vue_type_template_id_33c605fd___WEBPACK_IMPORTED_MODULE_0__.render,
  _Modal_vue_vue_type_template_id_33c605fd___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Components/Modal.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Tutors/Show.vue":
/*!********************************************!*\
  !*** ./resources/js/Pages/Tutors/Show.vue ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Show_vue_vue_type_template_id_4e7be154___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=4e7be154& */ "./resources/js/Pages/Tutors/Show.vue?vue&type=template&id=4e7be154&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Tutors/Show.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_4e7be154___WEBPACK_IMPORTED_MODULE_0__.render,
  _Show_vue_vue_type_template_id_4e7be154___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Tutors/Show.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Components/Modal.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Components/Modal.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Modal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Modal.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Components/Modal.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Modal_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Tutors/Show.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/Tutors/Show.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Tutors/Show.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Components/Modal.vue?vue&type=template&id=33c605fd&":
/*!********************************************************************************!*\
  !*** ./resources/js/Pages/Components/Modal.vue?vue&type=template&id=33c605fd& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Modal_vue_vue_type_template_id_33c605fd___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Modal_vue_vue_type_template_id_33c605fd___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Modal_vue_vue_type_template_id_33c605fd___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Modal.vue?vue&type=template&id=33c605fd& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Components/Modal.vue?vue&type=template&id=33c605fd&");


/***/ }),

/***/ "./resources/js/Pages/Tutors/Show.vue?vue&type=template&id=4e7be154&":
/*!***************************************************************************!*\
  !*** ./resources/js/Pages/Tutors/Show.vue?vue&type=template&id=4e7be154& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_4e7be154___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_4e7be154___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_4e7be154___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Show.vue?vue&type=template&id=4e7be154& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Tutors/Show.vue?vue&type=template&id=4e7be154&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Components/Modal.vue?vue&type=template&id=33c605fd&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Components/Modal.vue?vue&type=template&id=33c605fd& ***!
  \***********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("input", {
      staticClass: "modal-toggle",
      attrs: { type: "checkbox", id: "job-post-summary-modal" },
      domProps: { checked: _vm.show_modal },
    }),
    _vm._v(" "),
    _c("div", { staticClass: "modal" }, [
      _c(
        "div",
        { staticClass: "modal-box bg-white relative text-black" },
        [
          _c(
            "label",
            {
              staticClass: "btn btn-sm btn-circle absolute right-2 top-2",
              on: {
                click: function ($event) {
                  return _vm.$emit("closeModal")
                },
              },
            },
            [_vm._v("✕")]
          ),
          _vm._v(" "),
          _c("h2", { staticClass: "text-2xl" }, [_vm._t("title")], 2),
          _vm._v(" "),
          _vm._t("default"),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "modal-action" },
            [_vm._t("modal-actions")],
            2
          ),
        ],
        2
      ),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Tutors/Show.vue?vue&type=template&id=4e7be154&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Tutors/Show.vue?vue&type=template&id=4e7be154& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("Head", {
        attrs: {
          title: _vm.tutor.data.full_name,
          meta: "General page",
          "head-key": "general",
        },
      }),
      _vm._v(" "),
      _c("div", {}, [
        _c(
          "div",
          { staticClass: "card container mx-auto lg:my-20 shadow-md" },
          [
            _c("div", { staticClass: "w-full p-10 bg-background-3" }, [
              _c(
                "h1",
                {
                  staticClass:
                    "text-white text-center card-title font-bold text-4xl",
                },
                [_vm._v("\n          Tutor Details\n        ")]
              ),
              _vm._v(" "),
              _c("div", { staticClass: "grid justify-center text-white" }, [
                _c("div", { staticClass: "avatar justify-center" }, [
                  _c(
                    "div",
                    {
                      staticClass:
                        "mb-8 rounded-full h-28 ring ring-white ring-offset-base-100 ring-offset-2",
                    },
                    [
                      _c("img", {
                        attrs: { src: _vm.tutor.data.profile_picture_url },
                      }),
                    ]
                  ),
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "mb-5 text-center" }, [
                  _c("h1", { staticClass: "font-bold text-2xl" }, [
                    _vm._v(
                      "\n              " +
                        _vm._s(_vm.tutor.data.full_name) +
                        "\n            "
                    ),
                  ]),
                  _vm._v(" "),
                  _c("h3", [
                    _c(
                      "svg",
                      {
                        staticClass: "w-5 h-5 inline-block",
                        attrs: {
                          xmlns: "http://www.w3.org/2000/svg",
                          fill: "none",
                          viewBox: "0 0 24 24",
                          "stroke-width": "1.5",
                          stroke: "currentColor",
                        },
                      },
                      [
                        _c("path", {
                          attrs: {
                            "stroke-linecap": "round",
                            "stroke-linejoin": "round",
                            d: "M15 10.5a3 3 0 11-6 0 3 3 0 016 0z",
                          },
                        }),
                        _vm._v(" "),
                        _c("path", {
                          attrs: {
                            "stroke-linecap": "round",
                            "stroke-linejoin": "round",
                            d: "M19.5 10.5c0 7.142-7.5 11.25-7.5 11.25S4.5 17.642 4.5 10.5a7.5 7.5 0 1115 0z",
                          },
                        }),
                      ]
                    ),
                    _vm._v(
                      "\n              " +
                        _vm._s(_vm.tutor.data.state_of_residence) +
                        ",\n              " +
                        _vm._s(_vm.tutor.data.lga_of_residence) +
                        "\n            "
                    ),
                  ]),
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "flex justify-center mt-5" },
                  [
                    _c(
                      "Link",
                      {
                        staticClass:
                          "btn bg-accent-1 hover:bg-accent-1-hover text-white",
                        attrs: {
                          href:
                            "/tutors/show/request-form/" + _vm.tutor.data.id,
                        },
                      },
                      [_vm._v("\n              Hire Tutor\n            ")]
                    ),
                  ],
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "mt-3 w-80" }, [
                  _c("p", { staticClass: "text-1xl text-white text-center" }, [
                    _vm._v(
                      "\n              " +
                        _vm._s(_vm.tutor.data.bio) +
                        "\n            "
                    ),
                  ]),
                ]),
              ]),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-body text-black" }, [
              _c(
                "div",
                {
                  staticClass:
                    "md:grid md:grid-cols-3 md:gap-2 justify-center mb-10",
                },
                [
                  _c(
                    "div",
                    { staticClass: "card shadow-md w-auto lg:card-side" },
                    [
                      _c("div", { staticClass: "card-body" }, [
                        _c("h2", { staticClass: "card-title" }, [
                          _vm._v("Subjects"),
                        ]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          _vm._l(
                            _vm.tutor.data.subjects,
                            function (subject, index) {
                              return _c("li", { key: index }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(subject.name) +
                                    "\n                "
                                ),
                              ])
                            }
                          ),
                          0
                        ),
                      ]),
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "card shadow-md w-auto lg:card-side" },
                    [
                      _c("div", { staticClass: "card-body" }, [
                        _c("h2", { staticClass: "card-title" }, [
                          _vm._v("Teaching Skills"),
                        ]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          _vm._l(
                            _vm.tutor.data.skills,
                            function (skill, index) {
                              return _c("li", { key: index }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(skill.name) +
                                    "\n                "
                                ),
                              ])
                            }
                          ),
                          0
                        ),
                      ]),
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "card shadow-md w-auto lg:card-side" },
                    [
                      _c("div", { staticClass: "card-body" }, [
                        _c("h2", { staticClass: "card-title" }, [
                          _vm._v("Work Experience"),
                        ]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          _vm._l(_vm.tutor.data.jobs, function (job, index) {
                            return _c(
                              "li",
                              {
                                key: index,
                                staticClass: "flex flex-row justify-between",
                              },
                              [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(job.job_title) +
                                    "\n                  "
                                ),
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "inline-block btn btn-ghost btn-danger btn-sm",
                                    on: {
                                      click: function () {
                                        _vm.job_details_to_show = job
                                        _vm.show_job_details_modal = true
                                      },
                                    },
                                  },
                                  [
                                    _vm._v(
                                      "\n                    Read More\n                  "
                                    ),
                                  ]
                                ),
                              ]
                            )
                          }),
                          0
                        ),
                      ]),
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "card shadow-md w-auto lg:card-side" },
                    [
                      _c("div", { staticClass: "card-body" }, [
                        _c("h2", { staticClass: "card-title" }, [
                          _vm._v("Available On"),
                        ]),
                        _vm._v(" "),
                        _c(
                          "ul",
                          _vm._l(
                            _vm.tutor.data.available_days,
                            function (available_day, index) {
                              return _c("li", { key: index }, [
                                _vm._v(
                                  "\n                  " +
                                    _vm._s(available_day.name) +
                                    "\n                "
                                ),
                              ])
                            }
                          ),
                          0
                        ),
                      ]),
                    ]
                  ),
                ]
              ),
            ]),
          ]
        ),
      ]),
      _vm._v(" "),
      _c(
        "modal",
        {
          attrs: { show_modal: _vm.show_job_details_modal },
          on: {
            closeModal: function ($event) {
              _vm.show_job_details_modal = false
            },
          },
          scopedSlots: _vm._u([
            {
              key: "title",
              fn: function () {
                return [
                  _vm._v(
                    "\n      " +
                      _vm._s(_vm.job_details_to_show.job_title) +
                      "\n    "
                  ),
                ]
              },
              proxy: true,
            },
          ]),
        },
        [
          _vm._v(" "),
          _c("p", [
            _vm._v("Employer: " + _vm._s(_vm.job_details_to_show.employer)),
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "Employed For: " +
                _vm._s(_vm.job_details_to_show.employment_duration)
            ),
          ]),
          _vm._v(" "),
          _c("p", [
            _vm._v(
              "Job Description: " +
                _vm._s(_vm.job_details_to_show.job_description)
            ),
          ]),
        ]
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);