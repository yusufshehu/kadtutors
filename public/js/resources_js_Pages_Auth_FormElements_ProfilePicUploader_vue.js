"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Auth_FormElements_ProfilePicUploader_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import { required, minLength, between, email } from "vuelidate/lib/validators";

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  inject: ["toggleToast"],
  data: function data() {
    return {
      previewImage: null
    };
  },
  props: ["user"],
  name: "ProfilePicUploader",
  methods: {
    setPreviewImage: function setPreviewImage(image) {
      var _this = this;

      var reader = new FileReader();

      reader.onload = function (e) {
        _this.previewImage = e.target.result;
      };

      reader.readAsDataURL(image);
      this.$emit("input", image);
    },
    selectImage: function selectImage() {
      this.$refs.fileInput.click();
    },
    pickFile: function pickFile() {
      var _this2 = this;

      var input = this.$refs.fileInput;
      var file = input.files;

      if (file && file[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          _this2.previewImage = e.target.result;
        };

        reader.readAsDataURL(file[0]); // this.$emit("input", file[0]);

        this.updateUserProfilePicture(file[0]); // this.user.profile_picture = file[0];
        // this.image_validation_failed = false;
        // if (this.user_errors.has('image')) {
        //   this.user_errors.delete('image')
        // }
      } // console.log(this.tutor_details.image)

    },
    updateUserProfilePicture: function updateUserProfilePicture(image) {
      var _this3 = this;

      var fd = new FormData();
      fd.append("profile_picture", image);
      _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__.Inertia.post("/users/" + this.user.id + "/update-profile-pic", fd, {
        onStart: function onStart() {
          _this3.submitting_form = true;

          _this3.toggleToast("Updating", true);
        },
        onFinish: function onFinish() {
          _this3.submitting_form = false;

          _this3.toggleToast("Profile Picture Updated");
        },
        onError: function onError(errors) {
          _this3.toggleToast("Something went wrong", false, "danger"); //   for (let msg in errors.response.data.errors) {
          //     let i = 0;
          //     //text += person[x] + " ";
          //     this.errors.push(err.response.data.errors[msg][i]);
          //     i++;
          //   }
          //   console.log(this.errors);
          //   console.log(err.response.data.errors);
          //   this.submitting_form = false;

        }
      });
    }
  } //   validations: {
  //     previewImage: { required },
  //   },

});

/***/ }),

/***/ "./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ProfilePicUploader_vue_vue_type_template_id_897603b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProfilePicUploader.vue?vue&type=template&id=897603b6& */ "./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=template&id=897603b6&");
/* harmony import */ var _ProfilePicUploader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProfilePicUploader.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProfilePicUploader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProfilePicUploader_vue_vue_type_template_id_897603b6___WEBPACK_IMPORTED_MODULE_0__.render,
  _ProfilePicUploader_vue_vue_type_template_id_897603b6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfilePicUploader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ProfilePicUploader.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfilePicUploader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=template&id=897603b6&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=template&id=897603b6& ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfilePicUploader_vue_vue_type_template_id_897603b6___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfilePicUploader_vue_vue_type_template_id_897603b6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProfilePicUploader_vue_vue_type_template_id_897603b6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ProfilePicUploader.vue?vue&type=template&id=897603b6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=template&id=897603b6&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=template&id=897603b6&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Auth/FormElements/ProfilePicUploader.vue?vue&type=template&id=897603b6& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("input", {
      ref: "fileInput",
      attrs: { type: "file", hidden: "", accept: "image/png, image/jpeg" },
      on: {
        input: function ($event) {
          return _vm.pickFile()
        },
      },
    }),
    _vm._v(" "),
    _c("div", { staticClass: "grid justify-center mb-10" }, [
      _c("div", { staticClass: "avatar justify-center" }, [
        _c(
          "div",
          {
            staticClass:
              "rounded-full w-24 h-24 ring ring-black ring-offset-base-100 ring-offset-2",
          },
          [
            _vm.user.profile_picture_url == null
              ? _c("img", {
                  attrs: { src: "/images/blank-profile-picture.svg" },
                })
              : _vm._e(),
            _vm._v(" "),
            _vm.user.profile_picture_url
              ? _c("img", { attrs: { src: "" + _vm.user.profile_picture_url } })
              : _vm._e(),
          ]
        ),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "flex justify-center" }, [
        _c(
          "button",
          {
            staticClass: "btn mt-1",
            on: {
              click: function ($event) {
                return _vm.selectImage()
              },
            },
          },
          [
            _c(
              "svg",
              {
                staticClass: "w-6 h-6",
                attrs: {
                  xmlns: "http://www.w3.org/2000/svg",
                  fill: "none",
                  viewBox: "0 0 24 24",
                  "stroke-width": "1.5",
                  stroke: "currentColor",
                },
              },
              [
                _c("path", {
                  attrs: {
                    "stroke-linecap": "round",
                    "stroke-linejoin": "round",
                    d: "M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L6.832 19.82a4.5 4.5 0 01-1.897 1.13l-2.685.8.8-2.685a4.5 4.5 0 011.13-1.897L16.863 4.487zm0 0L19.5 7.125",
                  },
                }),
              ]
            ),
          ]
        ),
      ]),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);