"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_EditArticle_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/EditArticle.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/EditArticle.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _components_ArticleForm_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/ArticleForm.vue */ "./resources/js/Pages/components/ArticleForm.vue");
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    ArticleForm: _components_ArticleForm_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    article: Object
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      dialog: false,
      removing_file: false,
      adding_file: false,
      file_to_remove: ""
    };
  },
  methods: {
    openFileRemovalDialog: function openFileRemovalDialog(file) {
      this.dialog = true;
      this.file_to_remove = file;
    },
    viewAttachment: function viewAttachment(attachment_link) {
      window.open(attachment_link, "_blank");
    },
    addFile: function addFile() {
      var _this = this;

      var files = this.$refs.articleFiles.files;
      this.adding_file = true;
      var self = this;
      var formData = new FormData();

      for (var i = 0; i < this.$refs.articleFiles.files.length; i++) {
        var file = this.$refs.articleFiles.files[i];
        formData.append("article-file[" + i + "]", file);
        formData.append("file_name[" + i + "]", this.$refs.articleFiles.files[i].name);
      }

      axios.post("/article/".concat(this.article.id, "/upload-files"), formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }).then(function (response) {
        for (var _i = 0; _i < response.data.links.length; _i++) {
          self.article.files.push(response.data.links[_i]);
        }

        _this.adding_file = false;
      })["catch"](function (err) {
        _this.adding_file = false;
      });
    },
    removeAttachment: function removeAttachment(url) {
      var _this2 = this;

      var self = this;
      this.removing_file = true;
      axios["delete"]("/article/".concat(this.article.id, "/delete-file"), {
        params: {
          url: url
        }
      }).then(function (response) {
        console.log(response);
        self.article.files = [];

        for (var i = 0; i < response.data.links.length; i++) {
          self.article.files.push(response.data.links[i]);
        }

        _this2.dialog = false;
        _this2.removing_file = false;
      })["catch"](function (err) {
        _this2.dialog = false;
        _this2.removing_file = false;
      });
    }
  },
  props: ["article"]
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleForm.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleForm.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ArticleFiles_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ArticleFiles.vue */ "./resources/js/Pages/components/ArticleFiles.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import {
//     VueEditor
// } from "vue2-editor";



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  props: {
    update: {
      type: Boolean,
      "default": false
    },
    article: {
      type: Object,
      "default": function _default() {
        return {
          article: {}
        };
      }
    }
  },
  name: "ArticleForm",
  components: {
    VueEditor: VueEditor,
    ArticleFiles: _ArticleFiles_vue__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      //
      uploading: false,
      drafting_article: false,
      //
      rules: {
        required: function required(value) {
          return !!value || "Required.";
        } // counter: value => value.length <= 20 || 'Max 20 characters',

      }
    };
  },
  methods: {
    removeError: function removeError(name) {
      var errorName = name;

      if (this.$page.props.errors.name) {
        this.$page.props.errors.errorName = null;
      }
    },
    submitForm: function submitForm(status) {
      if (this.update) {
        this.updateArticle(status);
      } else {
        this.createArticle(status);
      }
    },
    createArticle: function createArticle(status) {
      var _this = this;

      this.article.status = status;
      _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__.Inertia.post("/articles", this.article, {
        onStart: function onStart() {
          _this.submitting_form = true;
        },
        onFinish: function onFinish() {
          _this.uploading = false;
          _this.drafting_article = false;
        }
      });
    },
    updateArticle: function updateArticle(status) {
      var _this2 = this;

      this.article.status = status;
      _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__.Inertia.patch("/articles/" + this.article.id, this.article, {
        onStart: function onStart() {
          _this2.submitting_form = true;
        },
        onFinish: function onFinish() {
          _this2.uploading = false;
          _this2.drafting_article = false;
        }
      });
    },
    deleteFile: function deleteFile(id, index) {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_1___default()["delete"]("article-files/" + id).then(function (res) {
        _this3.article.files.splice(index, 1);
      });
    },
    handleImageAdded: function handleImageAdded(file, Editor, cursorLocation, resetUploader) {
      // An example of using FormData
      // NOTE: Your key could be different such as:
      // formData.append('file', file)
      var InstructionsFormData = new FormData();
      InstructionsFormData.append("image", file); // console.log(InstructionsFormData)

      axios__WEBPACK_IMPORTED_MODULE_1___default().post("/articles/upload/body-image", InstructionsFormData, {}).then(function (result) {
        var url = result.data.url; // Get url from response

        Editor.insertEmbed(cursorLocation, "image", url);
        resetUploader();
      })["catch"](function (err) {});
    },
    onImageRemoved: function onImageRemoved(file, Editor, cursorLocation, resetUploader) {
      // when image in assignment instructions is removed
      var data = {
        path: file
      };
      axios__WEBPACK_IMPORTED_MODULE_1___default().post("/articles/delete/body-image", data).then(function (result) {});
    }
  }
});

/***/ }),

/***/ "./resources/js/Pages/EditArticle.vue":
/*!********************************************!*\
  !*** ./resources/js/Pages/EditArticle.vue ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _EditArticle_vue_vue_type_template_id_45b7ab1a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditArticle.vue?vue&type=template&id=45b7ab1a& */ "./resources/js/Pages/EditArticle.vue?vue&type=template&id=45b7ab1a&");
/* harmony import */ var _EditArticle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditArticle.vue?vue&type=script&lang=js& */ "./resources/js/Pages/EditArticle.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditArticle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditArticle_vue_vue_type_template_id_45b7ab1a___WEBPACK_IMPORTED_MODULE_0__.render,
  _EditArticle_vue_vue_type_template_id_45b7ab1a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/EditArticle.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/components/ArticleFiles.vue":
/*!********************************************************!*\
  !*** ./resources/js/Pages/components/ArticleFiles.vue ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArticleFiles.vue?vue&type=template&id=63d47321& */ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&");
/* harmony import */ var _ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleFiles.vue?vue&type=script&lang=js& */ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.render,
  _ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/components/ArticleFiles.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/components/ArticleForm.vue":
/*!*******************************************************!*\
  !*** ./resources/js/Pages/components/ArticleForm.vue ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ArticleForm_vue_vue_type_template_id_3aa9298a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArticleForm.vue?vue&type=template&id=3aa9298a& */ "./resources/js/Pages/components/ArticleForm.vue?vue&type=template&id=3aa9298a&");
/* harmony import */ var _ArticleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleForm.vue?vue&type=script&lang=js& */ "./resources/js/Pages/components/ArticleForm.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArticleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArticleForm_vue_vue_type_template_id_3aa9298a___WEBPACK_IMPORTED_MODULE_0__.render,
  _ArticleForm_vue_vue_type_template_id_3aa9298a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/components/ArticleForm.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/EditArticle.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/EditArticle.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditArticle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./EditArticle.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/EditArticle.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EditArticle_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ArticleFiles.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/components/ArticleForm.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/Pages/components/ArticleForm.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ArticleForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleForm.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/EditArticle.vue?vue&type=template&id=45b7ab1a&":
/*!***************************************************************************!*\
  !*** ./resources/js/Pages/EditArticle.vue?vue&type=template&id=45b7ab1a& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditArticle_vue_vue_type_template_id_45b7ab1a___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditArticle_vue_vue_type_template_id_45b7ab1a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_EditArticle_vue_vue_type_template_id_45b7ab1a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./EditArticle.vue?vue&type=template&id=45b7ab1a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/EditArticle.vue?vue&type=template&id=45b7ab1a&");


/***/ }),

/***/ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&":
/*!***************************************************************************************!*\
  !*** ./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321& ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ArticleFiles.vue?vue&type=template&id=63d47321& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&");


/***/ }),

/***/ "./resources/js/Pages/components/ArticleForm.vue?vue&type=template&id=3aa9298a&":
/*!**************************************************************************************!*\
  !*** ./resources/js/Pages/components/ArticleForm.vue?vue&type=template&id=3aa9298a& ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleForm_vue_vue_type_template_id_3aa9298a___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleForm_vue_vue_type_template_id_3aa9298a___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleForm_vue_vue_type_template_id_3aa9298a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ArticleForm.vue?vue&type=template&id=3aa9298a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleForm.vue?vue&type=template&id=3aa9298a&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/EditArticle.vue?vue&type=template&id=45b7ab1a&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/EditArticle.vue?vue&type=template&id=45b7ab1a& ***!
  \******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("article-form", { attrs: { article: _vm.article, update: true } })
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321& ***!
  \******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("input", {
        ref: "articleFiles",
        attrs: {
          type: "file",
          multiple: "",
          hidden: "",
          accept:
            "application/pdf,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.docx",
        },
        on: {
          input: function ($event) {
            return _vm.addFile()
          },
        },
      }),
      _vm._v(" "),
      _c(
        "h2",
        {
          staticClass: "headline mt-4",
          staticStyle: { "font-weight": "regular" },
        },
        [_vm._v("Files")]
      ),
      _vm._v(" "),
      _vm.article.files
        ? _c(
            "div",
            [
              _c(
                "v-list",
                { attrs: { subheader: "" } },
                [
                  _vm._l(_vm.article.files, function (file, index) {
                    return _c(
                      "v-list-item",
                      { key: index },
                      [
                        _vm._v(
                          "\n        " + _vm._s(file.file_name) + "\n\n        "
                        ),
                        _c("v-spacer"),
                        _vm._v(" "),
                        _c(
                          "v-list-item-action",
                          [
                            _c(
                              "v-row",
                              [
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { color: "blue", icon: "" },
                                    on: {
                                      click: function ($event) {
                                        return _vm.viewAttachment(file.file_url)
                                      },
                                    },
                                  },
                                  [_c("v-icon", [_vm._v(" mdi-eye")])],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { icon: "" },
                                    on: {
                                      click: function ($event) {
                                        return _vm.openFileRemovalDialog(
                                          file.file_url
                                        )
                                      },
                                    },
                                  },
                                  [
                                    _c("v-icon", { attrs: { color: "red" } }, [
                                      _vm._v(" mdi-delete"),
                                    ]),
                                  ],
                                  1
                                ),
                              ],
                              1
                            ),
                          ],
                          1
                        ),
                      ],
                      1
                    )
                  }),
                  _vm._v(" "),
                  _c("v-divider"),
                ],
                2
              ),
            ],
            1
          )
        : _c("div", [
            _c(
              "div",
              { staticClass: "mt-6" },
              [
                _c("v-icon", { attrs: { size: "50" } }, [
                  _vm._v(" mdi-border-none-variant "),
                ]),
                _vm._v(" "),
                _c("p", [_vm._v("No files")]),
              ],
              1
            ),
          ]),
      _vm._v(" "),
      _c(
        "v-row",
        { attrs: { align: "center" } },
        [
          _c(
            "v-col",
            [
              _c(
                "v-tooltip",
                {
                  attrs: { bottom: "" },
                  scopedSlots: _vm._u([
                    {
                      key: "activator",
                      fn: function (ref) {
                        var on = ref.on
                        var attrs = ref.attrs
                        return [
                          _c(
                            "v-btn",
                            _vm._g(
                              _vm._b(
                                {
                                  attrs: {
                                    color: "primary",
                                    small: "",
                                    outlined: "",
                                    loading: _vm.adding_file,
                                  },
                                  on: {
                                    click: function ($event) {
                                      return _vm.$refs.articleFiles.click()
                                    },
                                  },
                                },
                                "v-btn",
                                attrs,
                                false
                              ),
                              on
                            ),
                            [_vm._v("\n            Add File(s)\n          ")]
                          ),
                        ]
                      },
                    },
                  ]),
                },
                [_vm._v(" "), _c("span", [_vm._v("Upload new File(s)")])]
              ),
            ],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { attrs: { justify: "center" } },
        [
          _c(
            "v-dialog",
            {
              attrs: { persistent: "", "max-width": "290" },
              model: {
                value: _vm.dialog,
                callback: function ($$v) {
                  _vm.dialog = $$v
                },
                expression: "dialog",
              },
            },
            [
              _c(
                "v-card",
                [
                  _c(
                    "v-card-title",
                    { staticClass: "text-h5 text-center" },
                    [
                      _c(
                        "v-icon",
                        { attrs: { size: "50", color: "warning" } },
                        [_vm._v(" mdi-delete-alert ")]
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-card-text", [
                    _vm._v("Are you sure you want to remove this file?"),
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: {
                            color: "green darken-1",
                            text: "",
                            loading: _vm.removing_file,
                          },
                          on: {
                            click: function ($event) {
                              return _vm.removeAttachment(_vm.file_to_remove)
                            },
                          },
                        },
                        [_vm._v("\n            Yes\n          ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: {
                            color: "green darken-1",
                            text: "",
                            disabled: _vm.removing_file,
                          },
                          on: {
                            click: function ($event) {
                              _vm.dialog = false
                            },
                          },
                        },
                        [_vm._v("\n            No\n          ")]
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleForm.vue?vue&type=template&id=3aa9298a&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleForm.vue?vue&type=template&id=3aa9298a& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-card",
    { attrs: { elevation: "2" } },
    [
      _c("v-card-title", [_vm._v(" New Article ")]),
      _vm._v(" "),
      _c(
        "v-card-text",
        [
          _c(
            "v-row",
            [
              _c(
                "v-col",
                { attrs: { cols: "12", md: "6" } },
                [
                  _c("v-file-input", {
                    attrs: {
                      accept: "image/png, image/jpeg, image/bmp",
                      label: "Upload Banner",
                      chips: "",
                      outlined: "",
                      counter: "",
                      "show-size": 1000,
                      clearable: true,
                    },
                    model: {
                      value: _vm.article.banner,
                      callback: function ($$v) {
                        _vm.$set(_vm.article, "banner", $$v)
                      },
                      expression: "article.banner",
                    },
                  }),
                ],
                1
              ),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-row",
            [
              _c("v-col", { attrs: { cols: "12", md: "6" } }, [
                _c(
                  "div",
                  [
                    _c("v-text-field", {
                      attrs: {
                        rules: [_vm.rules.required],
                        label: "Article Title*",
                        outlined: "",
                      },
                      on: {
                        keydown: function ($event) {
                          _vm.$page.props.errors.title = null
                        },
                      },
                      model: {
                        value: _vm.article.title,
                        callback: function ($$v) {
                          _vm.$set(_vm.article, "title", $$v)
                        },
                        expression: "article.title",
                      },
                    }),
                    _vm._v(" "),
                    _vm.$page.props.errors.title
                      ? _c("div", {
                          staticClass: "error--text",
                          domProps: {
                            textContent: _vm._s(_vm.$page.props.errors.title),
                          },
                        })
                      : _vm._e(),
                  ],
                  1
                ),
              ]),
              _vm._v(" "),
              _c("v-col", { attrs: { cols: "12", md: "6" } }, [
                _c("div", [
                  _c(
                    "div",
                    [
                      _c("v-text-field", {
                        attrs: {
                          rules: [_vm.rules.required],
                          "persistent-hint": "",
                          hint: "Write a short description",
                          label: "Excerpt*",
                          outlined: "",
                        },
                        on: {
                          keydown: function ($event) {
                            _vm.$page.props.errors.excerpt = null
                          },
                        },
                        model: {
                          value: _vm.article.excerpt,
                          callback: function ($$v) {
                            _vm.$set(_vm.article, "excerpt", $$v)
                          },
                          expression: "article.excerpt",
                        },
                      }),
                      _vm._v(" "),
                      _vm.$page.props.errors.excerpt
                        ? _c("div", {
                            staticClass: "error--text mb-4",
                            domProps: {
                              textContent: _vm._s(
                                _vm.$page.props.errors.excerpt
                              ),
                            },
                          })
                        : _vm._e(),
                    ],
                    1
                  ),
                ]),
              ]),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "p-5" },
            [
              _c("VueEditor", {
                attrs: {
                  useCustomImageHandler: "",
                  id: "editor",
                  placeholder: "Type out your article",
                },
                on: {
                  keydown: function ($event) {
                    _vm.$page.props.errors.body = null
                  },
                  "image-removed": _vm.onImageRemoved,
                  "image-added": _vm.handleImageAdded,
                },
                model: {
                  value: _vm.article.body,
                  callback: function ($$v) {
                    _vm.$set(_vm.article, "body", $$v)
                  },
                  expression: "article.body",
                },
              }),
              _vm._v(" "),
              _vm.$page.props.errors.body
                ? _c("div", {
                    staticClass: "error--text",
                    domProps: {
                      textContent: _vm._s(_vm.$page.props.errors.body),
                    },
                  })
                : _vm._e(),
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            [
              _vm.update
                ? _c("article-files", { attrs: { article: _vm.article } })
                : _c(
                    "v-col",
                    { attrs: { cols: "12", md: "4", sm: "12" } },
                    [
                      _c("v-file-input", {
                        attrs: {
                          accept:
                            "application/pdf,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.docx",
                          label: "Upload File Attachments",
                          chips: "",
                          outlined: "",
                          counter: "",
                          "show-size": 1000,
                          clearable: true,
                          multiple: true,
                        },
                        model: {
                          value: _vm.article.attachments,
                          callback: function ($$v) {
                            _vm.$set(_vm.article, "attachments", $$v)
                          },
                          expression: "article.attachments",
                        },
                      }),
                    ],
                    1
                  ),
            ],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-card-actions",
        { staticClass: "pb-5" },
        [
          _c(
            "v-btn",
            {
              attrs: {
                color: "primary",
                disabled: _vm.uploading,
                loading: _vm.drafting_article,
                outlined: "",
              },
              on: {
                click: function ($event) {
                  return _vm.submitForm(1)
                },
              },
            },
            [_vm._v("\n      Save Draft\n    ")]
          ),
          _vm._v(" "),
          _c("v-spacer"),
          _vm._v(" "),
          _c(
            "div",
            [
              _c(
                "Link",
                {
                  attrs: {
                    as: "v-btn",
                    color: "primary",
                    outlined: "",
                    disabled: _vm.uploading || _vm.drafting_article,
                    href: _vm.update
                      ? "/articles/drafts"
                      : "/articles/published",
                  },
                },
                [_vm._v("\n        Cancel\n      ")]
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: {
                    color: "primary",
                    disabled: _vm.drafting_article,
                    loading: _vm.uploading,
                  },
                  on: {
                    click: function ($event) {
                      return _vm.submitForm(2)
                    },
                  },
                },
                [_vm._v("\n        Upload\n      ")]
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);