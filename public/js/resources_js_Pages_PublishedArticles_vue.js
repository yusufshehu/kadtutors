"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_PublishedArticles_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PublishedArticles.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PublishedArticles.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");
/* harmony import */ var _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-vue */ "./node_modules/@inertiajs/inertia-vue/dist/index.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  components: {
    Head: _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_1__.Head
  },
  created: function created() {// console.log("dks")
  },
  methods: {
    openSite: function openSite() {
      window.open('https://www.csdcltd.com', '_blank');
    },
    openUploadDialog: function openUploadDialog(id) {
      this.uploadDialog = true;
      this.article_to_upload = id;
    },
    getDrafts: function getDrafts() {
      var _this = this;

      _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__.Inertia.get('/articles?page=' + this.drafts.current_page, {
        preserveScroll: true,
        onFinish: function onFinish() {
          return _this.tabs = 1;
        },
        onSuccess: function onSuccess() {
          return _this.tabs = 1;
        }
      }); //  this.tabs = 1
    },
    uploadArticle: function uploadArticle() {
      var _this2 = this;

      _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__.Inertia.patch('/articles/' + this.article_to_upload + '/upload', {
        status: 2
      }, {
        preserveScroll: true,
        onFinish: function onFinish() {
          return _this2.tabs = 1;
        },
        onSuccess: function onSuccess() {
          return _this2.tabs = 1;
        }
      });
    },
    deleteArticle: function deleteArticle() {
      var _this3 = this;

      _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_0__.Inertia["delete"]('/articles/' + this.article_to_delete, {
        preserveScroll: true,
        onSuccess: function onSuccess() {
          return _this3.deleteDialog = false;
        }
      });
    },
    openDeleteDialog: function openDeleteDialog(id) {
      this.deleteDialog = true;
      this.article_to_delete = id;
    }
  },
  props: {
    'publishedArticles': Object
  },
  data: function data() {
    return {
      uploadDialog: false,
      deleteDialog: false,
      article_to_upload: '',
      article_to_delete: '',
      tabs: 0,
      search: "",
      headers: [{
        text: 'Title',
        align: 'start',
        sortable: false,
        value: 'title'
      }, {
        text: 'Excerpt',
        value: 'excerpt'
      }, {
        text: 'Published on',
        value: 'published_at'
      }, // {
      //     text: 'ID',
      //     value: 'carbs'
      // },
      // {
      //     text: 'Protein (g)',
      //     value: 'protein'
      // },
      {
        text: 'Actions',
        value: 'actions',
        sortable: false
      }]
    };
  }
});

/***/ }),

/***/ "./resources/js/Pages/PublishedArticles.vue":
/*!**************************************************!*\
  !*** ./resources/js/Pages/PublishedArticles.vue ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _PublishedArticles_vue_vue_type_template_id_30df8e72___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PublishedArticles.vue?vue&type=template&id=30df8e72& */ "./resources/js/Pages/PublishedArticles.vue?vue&type=template&id=30df8e72&");
/* harmony import */ var _PublishedArticles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PublishedArticles.vue?vue&type=script&lang=js& */ "./resources/js/Pages/PublishedArticles.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PublishedArticles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PublishedArticles_vue_vue_type_template_id_30df8e72___WEBPACK_IMPORTED_MODULE_0__.render,
  _PublishedArticles_vue_vue_type_template_id_30df8e72___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/PublishedArticles.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/PublishedArticles.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/Pages/PublishedArticles.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PublishedArticles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PublishedArticles.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PublishedArticles.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PublishedArticles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/PublishedArticles.vue?vue&type=template&id=30df8e72&":
/*!*********************************************************************************!*\
  !*** ./resources/js/Pages/PublishedArticles.vue?vue&type=template&id=30df8e72& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PublishedArticles_vue_vue_type_template_id_30df8e72___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PublishedArticles_vue_vue_type_template_id_30df8e72___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PublishedArticles_vue_vue_type_template_id_30df8e72___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./PublishedArticles.vue?vue&type=template&id=30df8e72& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PublishedArticles.vue?vue&type=template&id=30df8e72&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PublishedArticles.vue?vue&type=template&id=30df8e72&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/PublishedArticles.vue?vue&type=template&id=30df8e72& ***!
  \************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c("Head", {
        attrs: {
          title: "Published",
          meta: "published articles page",
          "head-key": "published",
        },
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "pa-5" },
        [
          _c("v-row", [_c("v-spacer")], 1),
          _vm._v(" "),
          _c(
            "v-card",
            { staticClass: "mt-5" },
            [
              _c(
                "v-card-title",
                [
                  _c(
                    "v-col",
                    [
                      _vm._v(
                        "\n                            Published Articles\n\n                            "
                      ),
                      _c("v-text-field", {
                        staticClass: "rounded-lg",
                        attrs: {
                          outlined: "",
                          "append-icon": "mdi-magnify",
                          label: "Search",
                          "single-line": "",
                          "hide-details": "",
                        },
                        model: {
                          value: _vm.search,
                          callback: function ($$v) {
                            _vm.search = $$v
                          },
                          expression: "search",
                        },
                      }),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "Link",
                    {
                      attrs: {
                        as: "v-btn",
                        href: "/articles/create",
                        large: "",
                        color: "primary",
                        outlined: "",
                      },
                    },
                    [
                      _vm._v(
                        "\n                        New Article\n                        "
                      ),
                    ]
                  ),
                ],
                1
              ),
              _vm._v(" "),
              _c("v-data-table", {
                attrs: {
                  headers: _vm.headers,
                  items: _vm.publishedArticles.data,
                  search: _vm.search,
                  "hide-default-footer": "",
                },
                scopedSlots: _vm._u([
                  {
                    key: "item.actions",
                    fn: function (ref) {
                      var item = ref.item
                      return [
                        _c(
                          "v-btn",
                          {
                            attrs: { icon: "" },
                            on: {
                              click: function ($event) {
                                return _vm.openSite()
                              },
                            },
                          },
                          [
                            _c("v-icon", { attrs: { color: "primary" } }, [
                              _vm._v(
                                "\n                                    mdi-open-in-app\n                                "
                              ),
                            ]),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "v-btn",
                          {
                            attrs: { icon: "" },
                            on: {
                              click: function ($event) {
                                return _vm.openDeleteDialog(item.id)
                              },
                            },
                          },
                          [
                            _c("v-icon", { attrs: { color: "error" } }, [
                              _vm._v(
                                "\n                                    mdi-delete\n                                "
                              ),
                            ]),
                          ],
                          1
                        ),
                      ]
                    },
                  },
                  {
                    key: "footer",
                    fn: function () {
                      return [
                        _c(
                          "div",
                          { staticClass: "text-center" },
                          [
                            _c("v-pagination", {
                              attrs: {
                                length: _vm.publishedArticles.last_page,
                                circle: "",
                              },
                              model: {
                                value: _vm.publishedArticles.current_page,
                                callback: function ($$v) {
                                  _vm.$set(
                                    _vm.publishedArticles,
                                    "current_page",
                                    $$v
                                  )
                                },
                                expression: "publishedArticles.current_page",
                              },
                            }),
                          ],
                          1
                        ),
                      ]
                    },
                    proxy: true,
                  },
                ]),
              }),
            ],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          model: {
            value: _vm.deleteDialog,
            callback: function ($$v) {
              _vm.deleteDialog = $$v
            },
            expression: "deleteDialog",
          },
        },
        [
          _c("v-card", [
            _c("div", { staticClass: " p-5 mb-5 mt-5 pt-5" }, [
              _c("h2", { staticClass: "text-h5 text-center" }, [
                _vm._v(
                  "\n        Are you sure you want to delete this article?\n        "
                ),
              ]),
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "pb-5" },
              [
                _c(
                  "v-btn",
                  {
                    attrs: { block: "", color: "error lighten-2", text: "" },
                    on: {
                      click: function ($event) {
                        _vm.deleteDialog = false
                      },
                    },
                  },
                  [_vm._v("\n            No\n          ")]
                ),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    attrs: { block: "", color: "green darken-1", text: "" },
                    on: {
                      click: function ($event) {
                        return _vm.deleteArticle()
                      },
                    },
                  },
                  [_vm._v("\n            Yes\n          ")]
                ),
              ],
              1
            ),
          ]),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          model: {
            value: _vm.uploadDialog,
            callback: function ($$v) {
              _vm.uploadDialog = $$v
            },
            expression: "uploadDialog",
          },
        },
        [
          _c("v-card", [
            _c("div", { staticClass: " p-5 mb-5 mt-5 pt-5" }, [
              _c("h2", { staticClass: "text-h5 text-center" }, [
                _vm._v(
                  "\n        Are you ready to publish this article?\n        "
                ),
              ]),
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "pb-5" },
              [
                _c(
                  "v-btn",
                  {
                    attrs: { block: "", color: "error lighten-2", text: "" },
                    on: {
                      click: function ($event) {
                        _vm.uploadDialog = false
                      },
                    },
                  },
                  [_vm._v("\n            No\n          ")]
                ),
                _vm._v(" "),
                _c(
                  "v-btn",
                  {
                    attrs: { block: "", color: "green darken-1", text: "" },
                    on: {
                      click: function ($event) {
                        return _vm.uploadArticle()
                      },
                    },
                  },
                  [_vm._v("\n            Yes\n          ")]
                ),
              ],
              1
            ),
          ]),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);