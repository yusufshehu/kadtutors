"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Account_Client_JobOffers_Show_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_AccountLayout_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../../Shared/AccountLayout.vue */ "./resources/js/Shared/AccountLayout.vue");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  layout: _Shared_AccountLayout_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
  props: _defineProperty({
    offer: {
      type: Object
    }
  }, "offer", {
    type: Object
  }),
  methods: {
    acceptOffer: function acceptOffer() {
      var _this = this;

      this.accepting_offer = true;
      var data = {
        offer_status: 2
      };
      axios.patch("/api/offer/" + this.$route.params.offer_id, data).then(function (response) {
        _this.accepting_offer = false;
      })["catch"](function (error) {
        _this.accepting_offer = false;
        _this.error.message = "Something went wrong";
      });
    }
  },
  data: function data() {
    return {
      offer: {},
      tutor: {},
      accepting_offer: false,
      error: {
        exists: false,
        message: null
      },
      show_request_acceptance_modal: false
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _Layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout */ "./resources/js/Shared/Layout.vue");
/* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");
/* harmony import */ var _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @inertiajs/inertia-vue */ "./node_modules/@inertiajs/inertia-vue/dist/index.js");
/* harmony import */ var _NavBar_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./NavBar.vue */ "./resources/js/Shared/NavBar.vue");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import AuthUser from "../../classes/AuthUser";





/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  name: "UserDetails",
  components: {
    Head: _inertiajs_inertia_vue__WEBPACK_IMPORTED_MODULE_3__.Head,
    NavBar: _NavBar_vue__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  layout: _Layout__WEBPACK_IMPORTED_MODULE_1__["default"],
  data: function data() {
    var _ref;

    return _ref = {
      error_message: null,
      loginError: false,
      user_roles: [],
      loading: false,
      error: {
        exists: false,
        message: null
      },
      user_is_logged_in: true,
      toast: {
        toast_text: "",
        show_toast: false,
        show_loader: false,
        type: ""
      },
      user: {
        email: "",
        password: ""
      },
      open: false
    }, _defineProperty(_ref, "user", {}), _defineProperty(_ref, "dimmer", true), _defineProperty(_ref, "right", false), _defineProperty(_ref, "logged_in", true), _defineProperty(_ref, "navItemsLeft", [{
      text: "Home",
      url: "/",
      name: "Home"
    }, {
      text: "Job Postings",
      url: "/job-posts",
      name: "Job Posts"
    }, {
      text: "Our Tutors",
      url: "/tutors",
      name: "Tutors"
    }, {
      text: "About Us",
      url: "/about-us",
      name: "About"
    } // {
    //   text: "Become A Tutor",
    //   url: "/become-a-tutor",
    //   name: "Become A Tutor",
    // },
    // {
    //   text: "Sign Up",
    //   url: "/signup",
    //   name: "Sign Up",
    // },
    ]), _ref;
  },
  provide: function provide() {
    return {
      toggleToast: this.toggleToast
    };
  },
  methods: {
    toggle: function toggle() {
      this.open = !this.open;
    },
    toggleToast: function toggleToast(text) {
      var _this = this;

      var show_loader = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "success";
      this.toast.show_toast = true;
      this.toast.toast_text = text;
      this.toast.show_loader = show_loader;
      this.toast.type = type;
      setTimeout(function () {
        _this.toast.show_toast = false;
      }, 3000);
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../../node_modules/css-loader/dist/runtime/api.js */ "./node_modules/css-loader/dist/runtime/api.js");
/* harmony import */ var _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0__);
// Imports

var ___CSS_LOADER_EXPORT___ = _node_modules_css_loader_dist_runtime_api_js__WEBPACK_IMPORTED_MODULE_0___default()(function(i){return i[1]});
// Module
___CSS_LOADER_EXPORT___.push([module.id, "\n#profile-nav-icon[data-v-7a0bde8b] {\n  width: 64px;\n}\n", ""]);
// Exports
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (___CSS_LOADER_EXPORT___);


/***/ }),

/***/ "./node_modules/css-loader/dist/runtime/api.js":
/*!*****************************************************!*\
  !*** ./node_modules/css-loader/dist/runtime/api.js ***!
  \*****************************************************/
/***/ ((module) => {



/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
// eslint-disable-next-line func-names
module.exports = function (cssWithMappingToString) {
  var list = []; // return the list of modules as css string

  list.toString = function toString() {
    return this.map(function (item) {
      var content = cssWithMappingToString(item);

      if (item[2]) {
        return "@media ".concat(item[2], " {").concat(content, "}");
      }

      return content;
    }).join("");
  }; // import a list of modules into the list
  // eslint-disable-next-line func-names


  list.i = function (modules, mediaQuery, dedupe) {
    if (typeof modules === "string") {
      // eslint-disable-next-line no-param-reassign
      modules = [[null, modules, ""]];
    }

    var alreadyImportedModules = {};

    if (dedupe) {
      for (var i = 0; i < this.length; i++) {
        // eslint-disable-next-line prefer-destructuring
        var id = this[i][0];

        if (id != null) {
          alreadyImportedModules[id] = true;
        }
      }
    }

    for (var _i = 0; _i < modules.length; _i++) {
      var item = [].concat(modules[_i]);

      if (dedupe && alreadyImportedModules[item[0]]) {
        // eslint-disable-next-line no-continue
        continue;
      }

      if (mediaQuery) {
        if (!item[2]) {
          item[2] = mediaQuery;
        } else {
          item[2] = "".concat(mediaQuery, " and ").concat(item[2]);
        }
      }

      list.push(item);
    }
  };

  return list;
};

/***/ }),

/***/ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !../../../node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js */ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js");
/* harmony import */ var _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_style_index_0_id_7a0bde8b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css& */ "./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css&");

            

var options = {};

options.insert = "head";
options.singleton = false;

var update = _node_modules_style_loader_dist_runtime_injectStylesIntoStyleTag_js__WEBPACK_IMPORTED_MODULE_0___default()(_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_style_index_0_id_7a0bde8b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"], options);



/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_style_index_0_id_7a0bde8b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_1__["default"].locals || {});

/***/ }),

/***/ "./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js":
/*!****************************************************************************!*\
  !*** ./node_modules/style-loader/dist/runtime/injectStylesIntoStyleTag.js ***!
  \****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {



var isOldIE = function isOldIE() {
  var memo;
  return function memorize() {
    if (typeof memo === 'undefined') {
      // Test for IE <= 9 as proposed by Browserhacks
      // @see http://browserhacks.com/#hack-e71d8692f65334173fee715c222cb805
      // Tests for existence of standard globals is to allow style-loader
      // to operate correctly into non-standard environments
      // @see https://github.com/webpack-contrib/style-loader/issues/177
      memo = Boolean(window && document && document.all && !window.atob);
    }

    return memo;
  };
}();

var getTarget = function getTarget() {
  var memo = {};
  return function memorize(target) {
    if (typeof memo[target] === 'undefined') {
      var styleTarget = document.querySelector(target); // Special case to return head of iframe instead of iframe itself

      if (window.HTMLIFrameElement && styleTarget instanceof window.HTMLIFrameElement) {
        try {
          // This will throw an exception if access to iframe is blocked
          // due to cross-origin restrictions
          styleTarget = styleTarget.contentDocument.head;
        } catch (e) {
          // istanbul ignore next
          styleTarget = null;
        }
      }

      memo[target] = styleTarget;
    }

    return memo[target];
  };
}();

var stylesInDom = [];

function getIndexByIdentifier(identifier) {
  var result = -1;

  for (var i = 0; i < stylesInDom.length; i++) {
    if (stylesInDom[i].identifier === identifier) {
      result = i;
      break;
    }
  }

  return result;
}

function modulesToDom(list, options) {
  var idCountMap = {};
  var identifiers = [];

  for (var i = 0; i < list.length; i++) {
    var item = list[i];
    var id = options.base ? item[0] + options.base : item[0];
    var count = idCountMap[id] || 0;
    var identifier = "".concat(id, " ").concat(count);
    idCountMap[id] = count + 1;
    var index = getIndexByIdentifier(identifier);
    var obj = {
      css: item[1],
      media: item[2],
      sourceMap: item[3]
    };

    if (index !== -1) {
      stylesInDom[index].references++;
      stylesInDom[index].updater(obj);
    } else {
      stylesInDom.push({
        identifier: identifier,
        updater: addStyle(obj, options),
        references: 1
      });
    }

    identifiers.push(identifier);
  }

  return identifiers;
}

function insertStyleElement(options) {
  var style = document.createElement('style');
  var attributes = options.attributes || {};

  if (typeof attributes.nonce === 'undefined') {
    var nonce =  true ? __webpack_require__.nc : 0;

    if (nonce) {
      attributes.nonce = nonce;
    }
  }

  Object.keys(attributes).forEach(function (key) {
    style.setAttribute(key, attributes[key]);
  });

  if (typeof options.insert === 'function') {
    options.insert(style);
  } else {
    var target = getTarget(options.insert || 'head');

    if (!target) {
      throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");
    }

    target.appendChild(style);
  }

  return style;
}

function removeStyleElement(style) {
  // istanbul ignore if
  if (style.parentNode === null) {
    return false;
  }

  style.parentNode.removeChild(style);
}
/* istanbul ignore next  */


var replaceText = function replaceText() {
  var textStore = [];
  return function replace(index, replacement) {
    textStore[index] = replacement;
    return textStore.filter(Boolean).join('\n');
  };
}();

function applyToSingletonTag(style, index, remove, obj) {
  var css = remove ? '' : obj.media ? "@media ".concat(obj.media, " {").concat(obj.css, "}") : obj.css; // For old IE

  /* istanbul ignore if  */

  if (style.styleSheet) {
    style.styleSheet.cssText = replaceText(index, css);
  } else {
    var cssNode = document.createTextNode(css);
    var childNodes = style.childNodes;

    if (childNodes[index]) {
      style.removeChild(childNodes[index]);
    }

    if (childNodes.length) {
      style.insertBefore(cssNode, childNodes[index]);
    } else {
      style.appendChild(cssNode);
    }
  }
}

function applyToTag(style, options, obj) {
  var css = obj.css;
  var media = obj.media;
  var sourceMap = obj.sourceMap;

  if (media) {
    style.setAttribute('media', media);
  } else {
    style.removeAttribute('media');
  }

  if (sourceMap && typeof btoa !== 'undefined') {
    css += "\n/*# sourceMappingURL=data:application/json;base64,".concat(btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))), " */");
  } // For old IE

  /* istanbul ignore if  */


  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    while (style.firstChild) {
      style.removeChild(style.firstChild);
    }

    style.appendChild(document.createTextNode(css));
  }
}

var singleton = null;
var singletonCounter = 0;

function addStyle(obj, options) {
  var style;
  var update;
  var remove;

  if (options.singleton) {
    var styleIndex = singletonCounter++;
    style = singleton || (singleton = insertStyleElement(options));
    update = applyToSingletonTag.bind(null, style, styleIndex, false);
    remove = applyToSingletonTag.bind(null, style, styleIndex, true);
  } else {
    style = insertStyleElement(options);
    update = applyToTag.bind(null, style, options);

    remove = function remove() {
      removeStyleElement(style);
    };
  }

  update(obj);
  return function updateStyle(newObj) {
    if (newObj) {
      if (newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap) {
        return;
      }

      update(obj = newObj);
    } else {
      remove();
    }
  };
}

module.exports = function (list, options) {
  options = options || {}; // Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
  // tags it will allow on a page

  if (!options.singleton && typeof options.singleton !== 'boolean') {
    options.singleton = isOldIE();
  }

  list = list || [];
  var lastIdentifiers = modulesToDom(list, options);
  return function update(newList) {
    newList = newList || [];

    if (Object.prototype.toString.call(newList) !== '[object Array]') {
      return;
    }

    for (var i = 0; i < lastIdentifiers.length; i++) {
      var identifier = lastIdentifiers[i];
      var index = getIndexByIdentifier(identifier);
      stylesInDom[index].references--;
    }

    var newLastIdentifiers = modulesToDom(newList, options);

    for (var _i = 0; _i < lastIdentifiers.length; _i++) {
      var _identifier = lastIdentifiers[_i];

      var _index = getIndexByIdentifier(_identifier);

      if (stylesInDom[_index].references === 0) {
        stylesInDom[_index].updater();

        stylesInDom.splice(_index, 1);
      }
    }

    lastIdentifiers = newLastIdentifiers;
  };
};

/***/ }),

/***/ "./resources/js/Pages/Account/Client/JobOffers/Show.vue":
/*!**************************************************************!*\
  !*** ./resources/js/Pages/Account/Client/JobOffers/Show.vue ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Show_vue_vue_type_template_id_319df1be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Show.vue?vue&type=template&id=319df1be& */ "./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=template&id=319df1be&");
/* harmony import */ var _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Show.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Show_vue_vue_type_template_id_319df1be___WEBPACK_IMPORTED_MODULE_0__.render,
  _Show_vue_vue_type_template_id_319df1be___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Account/Client/JobOffers/Show.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Shared/AccountLayout.vue":
/*!***********************************************!*\
  !*** ./resources/js/Shared/AccountLayout.vue ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _AccountLayout_vue_vue_type_template_id_7a0bde8b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true& */ "./resources/js/Shared/AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true&");
/* harmony import */ var _AccountLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./AccountLayout.vue?vue&type=script&lang=js& */ "./resources/js/Shared/AccountLayout.vue?vue&type=script&lang=js&");
/* harmony import */ var _AccountLayout_vue_vue_type_style_index_0_id_7a0bde8b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css& */ "./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");



;


/* normalize component */

var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _AccountLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _AccountLayout_vue_vue_type_template_id_7a0bde8b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render,
  _AccountLayout_vue_vue_type_template_id_7a0bde8b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  "7a0bde8b",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Shared/AccountLayout.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Show.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Shared/AccountLayout.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/Shared/AccountLayout.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AccountLayout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css&":
/*!********************************************************************************************************!*\
  !*** ./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css& ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_dist_cjs_js_node_modules_css_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_dist_cjs_js_clonedRuleSet_10_0_rules_0_use_2_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_style_index_0_id_7a0bde8b_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader/dist/cjs.js!../../../node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css& */ "./node_modules/style-loader/dist/cjs.js!./node_modules/css-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[1]!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/dist/cjs.js??clonedRuleSet-10[0].rules[0].use[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=style&index=0&id=7a0bde8b&scoped=true&lang=css&");


/***/ }),

/***/ "./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=template&id=319df1be&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=template&id=319df1be& ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_319df1be___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_319df1be___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Show_vue_vue_type_template_id_319df1be___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Show.vue?vue&type=template&id=319df1be& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=template&id=319df1be&");


/***/ }),

/***/ "./resources/js/Shared/AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true&":
/*!******************************************************************************************!*\
  !*** ./resources/js/Shared/AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true& ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_template_id_7a0bde8b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_template_id_7a0bde8b_scoped_true___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_AccountLayout_vue_vue_type_template_id_7a0bde8b_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=template&id=319df1be&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Account/Client/JobOffers/Show.vue?vue&type=template&id=319df1be& ***!
  \************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container card card-bordered shadow-xl" }, [
    _c("div", { staticClass: "flex flex-col card-body" }, [
      _c("h1", { staticClass: "text-3xl card-title font-bold" }, [
        _vm._v("\n      offer from Boss\n      "),
        _c("br"),
        _vm._v("\n\n      for\n      "),
        _c("span", { staticClass: "mt-10" }, [
          _vm._v(_vm._s(_vm.tutor.full_name)),
        ]),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "avatar mt-5" }, [
        _c(
          "div",
          {
            staticClass:
              "mb-8 rounded-full h-28 ring ring-black ring-offset-base-100 ring-offset-2",
          },
          [_c("img", { attrs: { src: _vm.tutor.profile_picture_url } })]
        ),
      ]),
      _vm._v(" "),
      _c("div", [
        _c("h2", { staticClass: "text-2xl font-semibold mb-5" }, [
          _vm._v("Offer Details"),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "gap-y-9" }, [
          _c("div", { staticClass: "container flex-row space-y-2" }, [
            _c("p", [
              _c("span", { staticClass: "font-semibold" }, [
                _vm._v("To tutor:"),
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.offer.number_of_students) +
                  " students\n          "
              ),
            ]),
            _vm._v(" "),
            _c(
              "p",
              [
                _c("span", { staticClass: "font-semibold" }, [
                  _vm._v("subjects:"),
                ]),
                _vm._v(" "),
                _vm._l(JSON.parse(_vm.offer.subjects), function (subject) {
                  return _c("span", { key: subject }, [
                    _vm._v(
                      "\n              " + _vm._s(subject) + ",\n            "
                    ),
                  ])
                }),
              ],
              2
            ),
            _vm._v(" "),
            _c("p", [
              _c("span", { staticClass: "font-semibold" }, [
                _vm._v(" Number Of Sessions Per Week:"),
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.offer.number_of_sessions_a_week) +
                  "\n          "
              ),
            ]),
            _vm._v(" "),
            _c(
              "p",
              [
                _c("span", { staticClass: "font-semibold" }, [
                  _vm._v(" Days For Sessions:"),
                ]),
                _vm._v(" "),
                _vm._l(JSON.parse(_vm.offer.session_days), function (day) {
                  return _c("span", { key: day }, [
                    _vm._v(
                      "\n              " + _vm._s(day) + ",\n            "
                    ),
                  ])
                }),
              ],
              2
            ),
            _vm._v(" "),
            _c("p", [
              _c("span", { staticClass: "font-semibold" }, [
                _vm._v(" Time of sessions:"),
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.offer.time_of_day) +
                  "\n          "
              ),
            ]),
            _vm._v(" "),
            _c("p", [
              _c("span", { staticClass: "font-semibold" }, [
                _vm._v(" Duration Per Session:"),
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.offer.duration_per_session) +
                  "\n          "
              ),
            ]),
            _vm._v(" "),
            _c("p", [
              _c("span", { staticClass: "font-semibold" }, [
                _vm._v("Agreement Duration:"),
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.offer.agreement_duration) +
                  " Months\n          "
              ),
            ]),
            _vm._v(" "),
            _c("p", [
              _c("span", { staticClass: "font-semibold" }, [
                _vm._v("Starting Date:"),
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.offer.starting_date) +
                  "\n          "
              ),
            ]),
            _vm._v(" "),
            _c("div", { staticStyle: { "margin-top": "2rem" } }, [
              _c("span", { staticClass: "font-normal" }, [_vm._v("Price")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("span", { staticClass: "text-4xl" }, [
                _vm._v(" ₦" + _vm._s(_vm.offer.proposed_price) + " "),
              ]),
            ]),
          ]),
          _vm._v(" "),
          _vm.offer.status !== "1"
            ? _c("div", [
                _c("input", {
                  staticClass: "modal-toggle",
                  attrs: { type: "checkbox" },
                  domProps: { checked: _vm.show_request_acceptance_modal },
                }),
                _vm._v(" "),
                _c("div", { staticClass: "modal" }, [
                  _c("div", { staticClass: "modal-box gap-y-9" }, [
                    _c("h1", { staticClass: "text-2xl font-bold mb-5" }, [
                      _vm._v(
                        "\n                Accept offer from " +
                          _vm._s(_vm.offer.offererName) +
                          "\n              "
                      ),
                    ]),
                    _vm._v(" "),
                    _vm.error.message
                      ? _c("p", { staticClass: "text-red-500" }, [
                          _vm._v(
                            "\n                " +
                              _vm._s(_vm.error.message) +
                              "\n              "
                          ),
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _c("div", { staticClass: "modal-action" }, [
                      _c(
                        "label",
                        {
                          staticClass: "btn btn-secondary btn-outline",
                          attrs: { disabled: _vm.accepting_offer },
                          on: {
                            click: function ($event) {
                              _vm.show_request_acceptance_modal = false
                            },
                          },
                        },
                        [_vm._v("No")]
                      ),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "btn btn-primary",
                          class: { loading: _vm.accepting_offer },
                          on: {
                            click: function ($event) {
                              return _vm.acceptOffer()
                            },
                          },
                        },
                        [_vm._v("Yes")]
                      ),
                    ]),
                  ]),
                ]),
              ])
            : _vm._e(),
        ]),
      ]),
    ]),
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Shared/AccountLayout.vue?vue&type=template&id=7a0bde8b&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "flex flex-row justify-center bg-white" },
    [
      _c("Head", { attrs: { title: "My Account" } }),
      _vm._v(" "),
      _c("div", { staticClass: "shadow drawer flex-none" }, [
        _c("input", {
          staticClass: "drawer-toggle",
          attrs: { id: "my-drawer", type: "checkbox" },
        }),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "drawer-content" },
          [
            _c("nav-bar"),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "flex flex-row justify-center md:px-28",
                staticStyle: { height: "min-content" },
              },
              [
                _c(
                  "div",
                  {
                    staticClass:
                      "card card-side bg-white shadow-xl text-black w-full",
                    staticStyle: { height: "110%" },
                  },
                  [
                    _c(
                      "ul",
                      {
                        staticClass:
                          "menu p-4 overflow-y-auto hidden md:block bg-white text-black mt-10",
                      },
                      [
                        _c(
                          "li",
                          [
                            _c(
                              "Link",
                              { attrs: { href: "/account/profile" } },
                              [
                                _c(
                                  "svg",
                                  {
                                    staticClass: "h-6 w-6",
                                    attrs: {
                                      xmlns: "http://www.w3.org/2000/svg",
                                      fill: "none",
                                      viewBox: "0 0 24 24",
                                      stroke: "currentColor",
                                      "stroke-width": "2",
                                    },
                                  },
                                  [
                                    _c("path", {
                                      attrs: {
                                        "stroke-linecap": "round",
                                        "stroke-linejoin": "round",
                                        d: "M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z",
                                      },
                                    }),
                                  ]
                                ),
                                _vm._v("Profile\n              "),
                              ]
                            ),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "li",
                          [
                            _c(
                              "Link",
                              { attrs: { href: "/account/job-offers" } },
                              [
                                _c(
                                  "svg",
                                  {
                                    staticClass: "w-6 h-6",
                                    attrs: {
                                      xmlns: "http://www.w3.org/2000/svg",
                                      fill: "none",
                                      viewBox: "0 0 24 24",
                                      "stroke-width": "1.5",
                                      stroke: "currentColor",
                                    },
                                  },
                                  [
                                    _c("path", {
                                      attrs: {
                                        "stroke-linecap": "round",
                                        "stroke-linejoin": "round",
                                        d: "M20.25 14.15v4.25c0 1.094-.787 2.036-1.872 2.18-2.087.277-4.216.42-6.378.42s-4.291-.143-6.378-.42c-1.085-.144-1.872-1.086-1.872-2.18v-4.25m16.5 0a2.18 2.18 0 00.75-1.661V8.706c0-1.081-.768-2.015-1.837-2.175a48.114 48.114 0 00-3.413-.387m4.5 8.006c-.194.165-.42.295-.673.38A23.978 23.978 0 0112 15.75c-2.648 0-5.195-.429-7.577-1.22a2.016 2.016 0 01-.673-.38m0 0A2.18 2.18 0 013 12.489V8.706c0-1.081.768-2.015 1.837-2.175a48.111 48.111 0 013.413-.387m7.5 0V5.25A2.25 2.25 0 0013.5 3h-3a2.25 2.25 0 00-2.25 2.25v.894m7.5 0a48.667 48.667 0 00-7.5 0M12 12.75h.008v.008H12v-.008z",
                                      },
                                    }),
                                  ]
                                ),
                                _vm._v(
                                  "\n                Job Offers\n              "
                                ),
                              ]
                            ),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm.$page.props.auth.user.roles.includes("Client")
                          ? _c(
                              "li",
                              [
                                _c(
                                  "Link",
                                  { attrs: { href: "/account/job-postings" } },
                                  [
                                    _c(
                                      "svg",
                                      {
                                        staticClass: "h-6 w-6",
                                        attrs: {
                                          xmlns: "http://www.w3.org/2000/svg",
                                          fill: "none",
                                          viewBox: "0 0 24 24",
                                          stroke: "currentColor",
                                          "stroke-width": "2",
                                        },
                                      },
                                      [
                                        _c("path", {
                                          attrs: {
                                            "stroke-linecap": "round",
                                            "stroke-linejoin": "round",
                                            d: "M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z",
                                          },
                                        }),
                                      ]
                                    ),
                                    _vm._v("Job Posts"),
                                  ]
                                ),
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("hr"),
                        _vm._v(" "),
                        _vm.$page.props.auth.user.roles.includes("Tutor")
                          ? _c(
                              "li",
                              [
                                _c(
                                  "Link",
                                  { attrs: { href: "/account/tutor-profile" } },
                                  [
                                    _c(
                                      "svg",
                                      {
                                        staticClass: "w-6 h-6",
                                        attrs: {
                                          xmlns: "http://www.w3.org/2000/svg",
                                          fill: "none",
                                          viewBox: "0 0 24 24",
                                          "stroke-width": "1.5",
                                          stroke: "currentColor",
                                        },
                                      },
                                      [
                                        _c("path", {
                                          attrs: {
                                            "stroke-linecap": "round",
                                            "stroke-linejoin": "round",
                                            d: "M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5",
                                          },
                                        }),
                                      ]
                                    ),
                                    _vm._v(
                                      "\n                Tutoring Details"
                                    ),
                                  ]
                                ),
                              ],
                              1
                            )
                          : _vm._e(),
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "ul",
                      {
                        staticClass: "menu md:hidden bg-white text-black mt-10",
                      },
                      [
                        _c(
                          "li",
                          [
                            _c(
                              "Link",
                              {
                                staticClass: "flex flex-col text-center",
                                attrs: { href: "/account/profile" },
                              },
                              [
                                _c(
                                  "svg",
                                  {
                                    staticClass: "h-6 w-6",
                                    attrs: {
                                      id: "profile-nav-icon",
                                      xmlns: "http://www.w3.org/2000/svg",
                                      fill: "none",
                                      viewBox: "0 0 24 24",
                                      stroke: "currentColor",
                                      "stroke-width": "2",
                                    },
                                  },
                                  [
                                    _c("path", {
                                      attrs: {
                                        "stroke-linecap": "round",
                                        "stroke-linejoin": "round",
                                        d: "M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z",
                                      },
                                    }),
                                  ]
                                ),
                                _vm._v(" "),
                                _c("span", [_vm._v(" Profile")]),
                              ]
                            ),
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm.$page.props.auth.user.roles.includes("Client")
                          ? _c(
                              "li",
                              [
                                _c(
                                  "Link",
                                  {
                                    staticClass: "flex flex-col text-center",
                                    attrs: { href: "/account/job-postings" },
                                  },
                                  [
                                    _c(
                                      "svg",
                                      {
                                        staticClass: "h-6 w-6",
                                        attrs: {
                                          xmlns: "http://www.w3.org/2000/svg",
                                          fill: "none",
                                          viewBox: "0 0 24 24",
                                          stroke: "currentColor",
                                          "stroke-width": "2",
                                        },
                                      },
                                      [
                                        _c("path", {
                                          attrs: {
                                            "stroke-linecap": "round",
                                            "stroke-linejoin": "round",
                                            d: "M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z",
                                          },
                                        }),
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("span", [_vm._v(" Job Posts")]),
                                  ]
                                ),
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _c("hr"),
                        _vm._v(" "),
                        _vm.$page.props.auth.user.roles.includes("Tutor")
                          ? _c(
                              "li",
                              [
                                _c(
                                  "Link",
                                  {
                                    staticClass: "flex flex-col text-center",
                                    attrs: { href: "/account/tutor-profile" },
                                  },
                                  [
                                    _c(
                                      "svg",
                                      {
                                        staticClass: "w-6 h-6",
                                        attrs: {
                                          xmlns: "http://www.w3.org/2000/svg",
                                          fill: "none",
                                          viewBox: "0 0 24 24",
                                          "stroke-width": "1.5",
                                          stroke: "currentColor",
                                        },
                                      },
                                      [
                                        _c("path", {
                                          attrs: {
                                            "stroke-linecap": "round",
                                            "stroke-linejoin": "round",
                                            d: "M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5",
                                          },
                                        }),
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c("span", [_vm._v(" Tutoring Details")]),
                                  ]
                                ),
                              ],
                              1
                            )
                          : _vm._e(),
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "divider divider-horizontal" }),
                    _vm._v(" "),
                    _c("div", { staticClass: "card-body" }, [
                      _c("h2", { staticClass: "card-title text-2xl" }, [
                        _vm._v(_vm._s(_vm.$page.url)),
                      ]),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c("div", {}, [_vm._t("default")], 2),
                    ]),
                  ]
                ),
              ]
            ),
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "drawer-side" }, [
          _c("label", {
            staticClass: "drawer-overlay",
            attrs: { for: "my-drawer" },
          }),
          _vm._v(" "),
          _c(
            "ul",
            {
              staticClass:
                "menu p-4 overflow-y-auto w-80 bg-white text-base-content",
            },
            [
              _c("div", [
                _c(
                  "label",
                  {
                    staticClass: "btn btn-square drawer-button",
                    attrs: { for: "my-drawer" },
                  },
                  [
                    _c(
                      "svg",
                      {
                        staticClass:
                          "inline-block w-4 h-4 stroke-current md:w-6 md:h-6",
                        attrs: {
                          xmlns: "http://www.w3.org/2000/svg",
                          fill: "none",
                          viewBox: "0 0 24 24",
                        },
                      },
                      [
                        _c("path", {
                          attrs: {
                            "stroke-linecap": "round",
                            "stroke-linejoin": "round",
                            "stroke-width": "2",
                            d: "M6 18L18 6M6 6l12 12",
                          },
                        }),
                      ]
                    ),
                  ]
                ),
              ]),
              _vm._v(" "),
              _vm._l(_vm.navItemsLeft, function (navItem, index) {
                return _c(
                  "li",
                  { key: index },
                  [
                    _c(
                      "Link",
                      {
                        staticClass:
                          "px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-black hover:opacity-75",
                        attrs: { href: navItem.url },
                      },
                      [
                        _vm._v(
                          "\n            " +
                            _vm._s(navItem.text) +
                            "\n          "
                        ),
                      ]
                    ),
                  ],
                  1
                )
              }),
            ],
            2
          ),
        ]),
      ]),
      _vm._v(" "),
      _c(
        "div",
        {
          directives: [
            {
              name: "show",
              rawName: "v-show",
              value: _vm.toast.show_toast,
              expression: "toast.show_toast",
            },
          ],
          staticClass: "toast transition delay-50",
        },
        [
          _c("div", { class: "alert alert-" + _vm.toast.type }, [
            _c("div", { staticClass: "flex flex-col" }, [
              _c("p", [_vm._v(_vm._s(_vm.toast.toast_text))]),
              _vm._v(" "),
              _c("progress", {
                directives: [
                  {
                    name: "show",
                    rawName: "v-show",
                    value: _vm.toast.show_loader,
                    expression: "toast.show_loader",
                  },
                ],
                staticClass: "progress w-56",
              }),
            ]),
          ]),
        ]
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);