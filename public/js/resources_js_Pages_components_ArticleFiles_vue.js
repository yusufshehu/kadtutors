"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_components_ArticleFiles_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  data: function data() {
    return {
      dialog: false,
      removing_file: false,
      adding_file: false,
      file_to_remove: ""
    };
  },
  methods: {
    openFileRemovalDialog: function openFileRemovalDialog(file) {
      this.dialog = true;
      this.file_to_remove = file;
    },
    viewAttachment: function viewAttachment(attachment_link) {
      window.open(attachment_link, "_blank");
    },
    addFile: function addFile() {
      var _this = this;

      var files = this.$refs.articleFiles.files;
      this.adding_file = true;
      var self = this;
      var formData = new FormData();

      for (var i = 0; i < this.$refs.articleFiles.files.length; i++) {
        var file = this.$refs.articleFiles.files[i];
        formData.append("article-file[" + i + "]", file);
        formData.append("file_name[" + i + "]", this.$refs.articleFiles.files[i].name);
      }

      axios.post("/article/".concat(this.article.id, "/upload-files"), formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }).then(function (response) {
        for (var _i = 0; _i < response.data.links.length; _i++) {
          self.article.files.push(response.data.links[_i]);
        }

        _this.adding_file = false;
      })["catch"](function (err) {
        _this.adding_file = false;
      });
    },
    removeAttachment: function removeAttachment(url) {
      var _this2 = this;

      var self = this;
      this.removing_file = true;
      axios["delete"]("/article/".concat(this.article.id, "/delete-file"), {
        params: {
          url: url
        }
      }).then(function (response) {
        console.log(response);
        self.article.files = [];

        for (var i = 0; i < response.data.links.length; i++) {
          self.article.files.push(response.data.links[i]);
        }

        _this2.dialog = false;
        _this2.removing_file = false;
      })["catch"](function (err) {
        _this2.dialog = false;
        _this2.removing_file = false;
      });
    }
  },
  props: ["article"]
});

/***/ }),

/***/ "./resources/js/Pages/components/ArticleFiles.vue":
/*!********************************************************!*\
  !*** ./resources/js/Pages/components/ArticleFiles.vue ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ArticleFiles.vue?vue&type=template&id=63d47321& */ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&");
/* harmony import */ var _ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ArticleFiles.vue?vue&type=script&lang=js& */ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.render,
  _ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/components/ArticleFiles.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ArticleFiles.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&":
/*!***************************************************************************************!*\
  !*** ./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321& ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ArticleFiles_vue_vue_type_template_id_63d47321___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./ArticleFiles.vue?vue&type=template&id=63d47321& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/components/ArticleFiles.vue?vue&type=template&id=63d47321& ***!
  \******************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("input", {
        ref: "articleFiles",
        attrs: {
          type: "file",
          multiple: "",
          hidden: "",
          accept:
            "application/pdf,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.docx",
        },
        on: {
          input: function ($event) {
            return _vm.addFile()
          },
        },
      }),
      _vm._v(" "),
      _c(
        "h2",
        {
          staticClass: "headline mt-4",
          staticStyle: { "font-weight": "regular" },
        },
        [_vm._v("Files")]
      ),
      _vm._v(" "),
      _vm.article.files
        ? _c(
            "div",
            [
              _c(
                "v-list",
                { attrs: { subheader: "" } },
                [
                  _vm._l(_vm.article.files, function (file, index) {
                    return _c(
                      "v-list-item",
                      { key: index },
                      [
                        _vm._v(
                          "\n        " + _vm._s(file.file_name) + "\n\n        "
                        ),
                        _c("v-spacer"),
                        _vm._v(" "),
                        _c(
                          "v-list-item-action",
                          [
                            _c(
                              "v-row",
                              [
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { color: "blue", icon: "" },
                                    on: {
                                      click: function ($event) {
                                        return _vm.viewAttachment(file.file_url)
                                      },
                                    },
                                  },
                                  [_c("v-icon", [_vm._v(" mdi-eye")])],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    attrs: { icon: "" },
                                    on: {
                                      click: function ($event) {
                                        return _vm.openFileRemovalDialog(
                                          file.file_url
                                        )
                                      },
                                    },
                                  },
                                  [
                                    _c("v-icon", { attrs: { color: "red" } }, [
                                      _vm._v(" mdi-delete"),
                                    ]),
                                  ],
                                  1
                                ),
                              ],
                              1
                            ),
                          ],
                          1
                        ),
                      ],
                      1
                    )
                  }),
                  _vm._v(" "),
                  _c("v-divider"),
                ],
                2
              ),
            ],
            1
          )
        : _c("div", [
            _c(
              "div",
              { staticClass: "mt-6" },
              [
                _c("v-icon", { attrs: { size: "50" } }, [
                  _vm._v(" mdi-border-none-variant "),
                ]),
                _vm._v(" "),
                _c("p", [_vm._v("No files")]),
              ],
              1
            ),
          ]),
      _vm._v(" "),
      _c(
        "v-row",
        { attrs: { align: "center" } },
        [
          _c(
            "v-col",
            [
              _c(
                "v-tooltip",
                {
                  attrs: { bottom: "" },
                  scopedSlots: _vm._u([
                    {
                      key: "activator",
                      fn: function (ref) {
                        var on = ref.on
                        var attrs = ref.attrs
                        return [
                          _c(
                            "v-btn",
                            _vm._g(
                              _vm._b(
                                {
                                  attrs: {
                                    color: "primary",
                                    small: "",
                                    outlined: "",
                                    loading: _vm.adding_file,
                                  },
                                  on: {
                                    click: function ($event) {
                                      return _vm.$refs.articleFiles.click()
                                    },
                                  },
                                },
                                "v-btn",
                                attrs,
                                false
                              ),
                              on
                            ),
                            [_vm._v("\n            Add File(s)\n          ")]
                          ),
                        ]
                      },
                    },
                  ]),
                },
                [_vm._v(" "), _c("span", [_vm._v("Upload new File(s)")])]
              ),
            ],
            1
          ),
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { attrs: { justify: "center" } },
        [
          _c(
            "v-dialog",
            {
              attrs: { persistent: "", "max-width": "290" },
              model: {
                value: _vm.dialog,
                callback: function ($$v) {
                  _vm.dialog = $$v
                },
                expression: "dialog",
              },
            },
            [
              _c(
                "v-card",
                [
                  _c(
                    "v-card-title",
                    { staticClass: "text-h5 text-center" },
                    [
                      _c(
                        "v-icon",
                        { attrs: { size: "50", color: "warning" } },
                        [_vm._v(" mdi-delete-alert ")]
                      ),
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-card-text", [
                    _vm._v("Are you sure you want to remove this file?"),
                  ]),
                  _vm._v(" "),
                  _c(
                    "v-card-actions",
                    [
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: {
                            color: "green darken-1",
                            text: "",
                            loading: _vm.removing_file,
                          },
                          on: {
                            click: function ($event) {
                              return _vm.removeAttachment(_vm.file_to_remove)
                            },
                          },
                        },
                        [_vm._v("\n            Yes\n          ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          attrs: {
                            color: "green darken-1",
                            text: "",
                            disabled: _vm.removing_file,
                          },
                          on: {
                            click: function ($event) {
                              _vm.dialog = false
                            },
                          },
                        },
                        [_vm._v("\n            No\n          ")]
                      ),
                    ],
                    1
                  ),
                ],
                1
              ),
            ],
            1
          ),
        ],
        1
      ),
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);