"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_About_vue"],{

/***/ "./resources/js/Pages/About.vue":
/*!**************************************!*\
  !*** ./resources/js/Pages/About.vue ***!
  \**************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _About_vue_vue_type_template_id_169e1534___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./About.vue?vue&type=template&id=169e1534& */ "./resources/js/Pages/About.vue?vue&type=template&id=169e1534&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _About_vue_vue_type_template_id_169e1534___WEBPACK_IMPORTED_MODULE_0__.render,
  _About_vue_vue_type_template_id_169e1534___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/About.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/About.vue?vue&type=template&id=169e1534&":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/About.vue?vue&type=template&id=169e1534& ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_About_vue_vue_type_template_id_169e1534___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_About_vue_vue_type_template_id_169e1534___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_About_vue_vue_type_template_id_169e1534___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./About.vue?vue&type=template&id=169e1534& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/About.vue?vue&type=template&id=169e1534&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/About.vue?vue&type=template&id=169e1534&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/About.vue?vue&type=template&id=169e1534& ***!
  \************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "container py-40 mx-auto text-black" }, [
        _c("div", { staticClass: "flex justify-center" }, [
          _c("div", [
            _c("h1", { staticClass: "text-6xl font-bold" }, [
              _vm._v("About Contactutors"),
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "lg:w-5/12 text-2xl mt-5" }, [
              _vm._v(
                "\n          Contactutors was borne out of a concern over the irregular and\n          sometimes, incredible fees charged by home tutors.\n        "
              ),
            ]),
          ]),
        ]),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "py-40 mx-auto bg-background text-black" }, [
        _c("div", { staticClass: "text-center" }, [
          _c("h1", { staticClass: "text-black text-6xl font-bold" }, [
            _vm._v("Our Mission"),
          ]),
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "flex justify-center" }, [
          _c("p", { staticClass: "text-center lg:w-5/12 text-2xl mt-5" }, [
            _vm._v(
              "\n        To provide students with an excellent tutoring experience at a cost\n        best suited for them. And tutors, with oppurtunities to continue\n        helping students grow in any way they can.\n      "
            ),
          ]),
        ]),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container py-40 mx-auto text-black" }, [
        _c("div", { staticClass: "text-center pb-10" }, [
          _c("h1", { staticClass: "text-black text-6xl font-bold" }, [
            _vm._v("Get To Know Us"),
          ]),
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "md:flex md:justify-center sm:grid sm:grid-cols-1" },
          [
            _c("div", { staticClass: "text-center p-10 mt-4" }, [
              _c("img", { attrs: { src: "images/ceo.png", height: "100" } }),
              _vm._v(" "),
              _c("p", { staticClass: "pt-5 font-bold" }, [
                _vm._v("CEO, Mr. Bakare Samuel"),
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "font-normal" }, [_vm._v("Teacher")]),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "text-center font-bold p-10" }, [
              _c("img", { attrs: { src: "images/co-founder.png" } }),
              _vm._v(" "),
              _c("p", { staticClass: "pt-5" }, [
                _vm._v("CTO & Co-Founder, Yusuf Shehu"),
              ]),
            ]),
          ]
        ),
      ]),
    ])
  },
]
render._withStripped = true



/***/ })

}]);