"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Layout_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Shared/Layout.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _classes_AuthUser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../classes/AuthUser */ "./resources/js/classes/AuthUser.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  created: function created() {
    var _this = this;

    _classes_AuthUser__WEBPACK_IMPORTED_MODULE_0__["default"].authCheck().then(function (response) {
      _this.logged_in = true;
      _this.user = response.data.user;
      _this.user_roles = response.data.roles; // next();
    })["catch"](function () {//    next('/login');
    });
  },
  name: "App",
  data: function data() {
    return {
      showMenu: false,
      user_roles: [],
      open_user_details_modal: false,
      user: "",
      logged_in: false,
      navItemsLeft: [{
        text: "Home",
        url: "/home",
        name: "Home"
      }, {
        text: "Jobs",
        url: "/jobs",
        name: "Jobs"
      }, // {
      //   text: "Contact",
      // },
      {
        text: "Our Tutors",
        url: "/tutors",
        name: "Tutors"
      }, {
        text: "About Us",
        url: "/about-us",
        name: "About"
      } // {
      //   text: "Become A Tutor",
      //   url: "/become-a-tutor",
      //   name: "Become A Tutor",
      // },
      // {
      //   text: "Sign Up",
      //   url: "/signup",
      //   name: "Sign Up",
      // },
      ]
    };
  },
  methods: {
    toggleNavbar: function toggleNavbar() {
      this.showMenu = !this.showMenu;
    } // logout() {
    //   //   AuthUser.authCheck().then((response) => {
    //   axios
    //     .post("/api/logout")
    //     .then((response) => {
    //       this.$router.replace("/login");
    //     })
    //     .catch((err) => {});
    //   //   });
    //   // $user =
    // },

  }
});

/***/ }),

/***/ "./resources/js/classes/AuthUser.js":
/*!******************************************!*\
  !*** ./resources/js/classes/AuthUser.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  authCheck: function authCheck() {
    return new Promise(function (resolve, reject) {
      // eslint-disable-next-line no-undef
      axios.get('/api/user').then(function (response) {
        resolve(response);
      })["catch"](function (error) {
        reject(error);
      });
    });
  }
});

/***/ }),

/***/ "./resources/js/Pages/Shared/Layout.vue":
/*!**********************************************!*\
  !*** ./resources/js/Pages/Shared/Layout.vue ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Layout_vue_vue_type_template_id_7a4e975b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Layout.vue?vue&type=template&id=7a4e975b& */ "./resources/js/Pages/Shared/Layout.vue?vue&type=template&id=7a4e975b&");
/* harmony import */ var _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Layout.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Shared/Layout.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Layout_vue_vue_type_template_id_7a4e975b___WEBPACK_IMPORTED_MODULE_0__.render,
  _Layout_vue_vue_type_template_id_7a4e975b___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Shared/Layout.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Shared/Layout.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Layout.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5[0].rules[0].use[0]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Shared/Layout.vue?vue&type=script&lang=js&");
 /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (_node_modules_babel_loader_lib_index_js_clonedRuleSet_5_0_rules_0_use_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Shared/Layout.vue?vue&type=template&id=7a4e975b&":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout.vue?vue&type=template&id=7a4e975b& ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_7a4e975b___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_7a4e975b___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Layout_vue_vue_type_template_id_7a4e975b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Layout.vue?vue&type=template&id=7a4e975b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Shared/Layout.vue?vue&type=template&id=7a4e975b&");


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Shared/Layout.vue?vue&type=template&id=7a4e975b&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/Pages/Shared/Layout.vue?vue&type=template&id=7a4e975b& ***!
  \********************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function () {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { attrs: { id: "app" } }, [
    _c("div", { staticClass: "shadow drawer flex-none" }, [
      _c("input", {
        staticClass: "drawer-toggle",
        attrs: { id: "my-drawer", type: "checkbox" },
      }),
      _vm._v(" "),
      _c("div", { staticClass: "drawer-content" }, [
        _c(
          "div",
          {
            staticClass:
              "w-full navbar sticky top-0 lg:hidden shadow-lg text-neutral-content z-10 bg-white",
          },
          [
            _c("div", { staticClass: "flex-none lg:hidden" }, [
              _c(
                "label",
                {
                  staticClass: "btn btn-square drawer-button",
                  attrs: { for: "my-drawer" },
                },
                [
                  _c(
                    "svg",
                    {
                      staticClass: "inline-block w-6 h-6 stroke-current",
                      attrs: {
                        xmlns: "http://www.w3.org/2000/svg",
                        fill: "none",
                        viewBox: "0 0 24 24",
                      },
                    },
                    [
                      _c("path", {
                        attrs: {
                          "stroke-linecap": "round",
                          "stroke-linejoin": "round",
                          "stroke-width": "2",
                          d: "M4 6h16M4 12h16M4 18h16",
                        },
                      }),
                    ]
                  ),
                ]
              ),
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "flex-1 navbar-end px-2 mx-2" }, [
              _vm.logged_in
                ? _c("div", { staticClass: "flex flex-row" }, [
                    _c("p", { staticClass: "text-black mt-4" }, [
                      _vm._v(
                        "\n              " +
                          _vm._s(_vm.user.name) +
                          "\n            "
                      ),
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "dropdown dropdown-end" }, [
                      _c(
                        "div",
                        {
                          staticClass:
                            "text-black m-1 btn btn-square btn-ghost",
                          attrs: { tabindex: "0" },
                        },
                        [
                          _c("span", [
                            _c(
                              "svg",
                              {
                                staticClass:
                                  "inline-block w-6 h-6 stroke-current",
                                attrs: {
                                  xmlns: "http://www.w3.org/2000/svg",
                                  fill: "none",
                                  viewBox: "0 0 24 24",
                                },
                              },
                              [
                                _c("path", {
                                  attrs: {
                                    "stroke-linecap": "round",
                                    "stroke-linejoin": "round",
                                    "stroke-width": "2",
                                    d: "M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z",
                                  },
                                }),
                              ]
                            ),
                          ]),
                        ]
                      ),
                    ]),
                  ])
                : _c("span", [_vm._m(0)]),
            ]),
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass:
              "navbar hidden lg:flex sticky top-0 py-5 mb-2 px-5 bg-white shadow-md text-neutral-content w-full z-10",
          },
          [
            _vm._m(1),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "flex-none hidden px-2 mx-2 lg:flex navbar-center",
              },
              [
                !_vm.logged_in
                  ? _c("div", { staticClass: "flex items-stretch" })
                  : _vm._e(),
              ]
            ),
            _vm._v(" "),
            !_vm.logged_in
              ? _c("div", { staticClass: "flex-none hidden lg:flex" })
              : _vm._e(),
            _vm._v(" "),
            _vm.logged_in
              ? _c("div", { staticClass: "flex-none hidden lg:flex" }, [
                  _c("p", { staticClass: "text-black" }, [
                    _vm._v(
                      "\n            " + _vm._s(_vm.user.name) + "\n          "
                    ),
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "dropdown dropdown-end" }, [
                    _c(
                      "div",
                      {
                        staticClass: "text-black m-1 btn btn-square btn-ghost",
                        attrs: { tabindex: "0" },
                      },
                      [
                        _c("span", [
                          _c(
                            "svg",
                            {
                              staticClass:
                                "inline-block w-6 h-6 stroke-current",
                              attrs: {
                                xmlns: "http://www.w3.org/2000/svg",
                                fill: "none",
                                viewBox: "0 0 24 24",
                              },
                            },
                            [
                              _c("path", {
                                attrs: {
                                  "stroke-linecap": "round",
                                  "stroke-linejoin": "round",
                                  "stroke-width": "2",
                                  d: "M5 12h.01M12 12h.01M19 12h.01M6 12a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0zm7 0a1 1 0 11-2 0 1 1 0 012 0z",
                                },
                              }),
                            ]
                          ),
                        ]),
                      ]
                    ),
                  ]),
                ])
              : _vm._e(),
          ]
        ),
        _vm._v(" "),
        _c("main", {}, [_vm._t("default")], 2),
        _vm._v(" "),
        _c(
          "footer",
          {
            staticClass: "bg-footer-bg w-full footer mt-4",
            staticStyle: { width: "100%", position: "static" },
          },
          [
            _c(
              "div",
              {
                staticClass:
                  "max-w-screen-xl px-4 py-12 mx-auto space-y-8 overflow-hidden sm:px-6 lg:px-8 text-white",
              },
              [
                !_vm.logged_in
                  ? _c(
                      "nav",
                      {
                        staticClass:
                          "flex flex-wrap justify-center -mx-5 -my-2 text-white",
                      },
                      _vm._l(_vm.navItemsLeft, function (navItem, index) {
                        return _c("div", {
                          key: index,
                          staticClass: "px-5 py-2 text-white",
                        })
                      }),
                      0
                    )
                  : _vm._e(),
                _vm._v(" "),
                _vm._m(2),
                _vm._v(" "),
                _c(
                  "p",
                  {
                    staticClass:
                      "mt-8 text-base leading-6 text-center text-gray-400",
                  },
                  [
                    _vm._v(
                      "\n            © 2021 Contactutors. All rights reserved.\n          "
                    ),
                  ]
                ),
              ]
            ),
          ]
        ),
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "drawer-side" }, [
        _c("label", {
          staticClass: "drawer-overlay",
          attrs: { for: "my-drawer" },
        }),
        _vm._v(" "),
        _c(
          "ul",
          {
            staticClass:
              "menu p-4 overflow-y-auto w-80 bg-base-100 text-base-content",
          },
          [
            _c("div", [
              _c(
                "label",
                {
                  staticClass: "btn btn-square drawer-button",
                  attrs: { for: "my-drawer" },
                },
                [
                  _c(
                    "svg",
                    {
                      staticClass:
                        "inline-block w-4 h-4 stroke-current md:w-6 md:h-6",
                      attrs: {
                        xmlns: "http://www.w3.org/2000/svg",
                        fill: "none",
                        viewBox: "0 0 24 24",
                      },
                    },
                    [
                      _c("path", {
                        attrs: {
                          "stroke-linecap": "round",
                          "stroke-linejoin": "round",
                          "stroke-width": "2",
                          d: "M6 18L18 6M6 6l12 12",
                        },
                      }),
                    ]
                  ),
                ]
              ),
            ]),
          ]
        ),
      ]),
    ]),
  ])
}
var staticRenderFns = [
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "/" } }, [
      _c("img", {
        attrs: { src: "/images/glasses.svg", width: "0", height: "10" },
      }),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "flex-1 px-2 mx-2" }, [
      _c("div", { staticClass: "flex flex-row space-x-4" }, [
        _c("a", { attrs: { href: "/home" } }, [
          _c("img", {
            attrs: { src: "images/glasses.svg", width: "80", height: "10" },
          }),
        ]),
      ]),
    ])
  },
  function () {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("a", { attrs: { href: "mailto:contactutors@gmail.com" } }, [
        _vm._v("Mail Us"),
      ]),
    ])
  },
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ normalizeComponent)
/* harmony export */ });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ })

}]);