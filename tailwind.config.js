module.exports = {
   purge: [
     './resources/**/*.blade.php',
     './resources/**/*.js',
     './resources/**/*.vue',
   ],

  theme: {
    extend: {
          colors: {
        'primary': '#EC615B',

        'accent-1': '#EC615B',
            'accent-1-hover': '#c91d18',
            'footer-bg': '#161616',
            'accent-2': "#3A1DA3",
            'accent-2-hover': "#5222D0",
            'background-3': "#333333",

        // 'accent-2': '#758AF5',
        // 'accent-3': '#2C418C',
        'background-2': '#F0FAF0',
         'background' : "#F5F5F5"

          },
          fontFamily: {
               'body': ['"Quicksand"']
          }
    },
  },
  variants: {
    extend: {},
  },
    plugins: [
      require('daisyui'),
  ],
}
