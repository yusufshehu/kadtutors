<?php

return [
    'tutor_application'         => [
        'statuses' => [
            1 => 'Pending',
            2 => 'Accepted',
            3 => 'Rejected',
        ],
    ],
    'offers'         => [
        'statuses' => [
            1 => 'Pending',
            2 => 'Accepted',
            3 => 'Rejected',
            4 => 'Negotiating',
            5 => 'Ongoing',
        ],
    ],
    'subjects' => [
        1 => [
            'text' => "Maths",
            'value' => 1
        ],
        2 => [
            'text' => "English",
            'value' => 2
        ],
        3 => [
            'text' => "Basic Science",
            'value' => 3
        ],
        4 => [
            'text' => "Technical Drawing",
            'value' => 4
        ],
        5 => [
            'text' => "Literature in English",
            'value' => 5
        ],
        6 => [
            'text' => "Phonics",
            'value' => 6
        ],
        7 => [
            'text' => "Physics",
            'value' => 7
        ],
        8 => [
            'text' => "Chemistry",
            'value' => 8
        ],
        9 => [
            'text' => "Further Mathematics",
            'value' => 9
        ],
        10 => [
            'text' => "Economics",
            'value' => 10
        ],
        11 => [
            'text' => "French",
            'value' => 11
        ],
        12 => [
            'text' => "Yoruba",
            'value' => 12
        ],
        13 => [
            'text' => "PHE",
            'value' => 13
        ],
        14 => [
            'text' => "Numeracy",
            'value' => 14
        ],
        15 => [
            'text' => "Fine Arts",
            'value' => 15
        ]
    ],
    'payment_intervals' => [
        1  => "Monthly",
        2 => "Weekly"
    ],

    'skills' => [
        1 => [
            'name' => 'I can speak more than one language',
            'id' => 1
        ],
        2 => [
            'name' => 'Active Listening',
            'id' => 2
        ],
        3 => [
            'name' => "I'm a good communicator",
            'id' => 3
        ],
        4 => [
            'name' => 'Technical Knowledge',
            'id' => 4
        ],
        5 => [
            'name' => 'Time Management',
            'id' => 5
        ],
        6 => [
            'name' => 'Problem-Solving',
            'id' => 6
        ],
        7 => [
            'name' => 'Patience',
            'id' => 7
        ],
        8 => [
            'name' => 'Positivity',
            'id' => 8
        ],
        9 => [
            'name' => 'Empathy',
            'id' => 9
        ],
    ]

];
