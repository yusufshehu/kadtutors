<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TutorApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TutorApplication::create(
            [
                'tutor_id' => $this->id,
                // 'status' => config('enums.tutor_application.statuses')[0]
                'status' => 1
            ]
        );
    }
}
