<?php

namespace Database\Seeders;

use App\Models\Skill;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = [
            "I can speak more than one language",
            "Active Listening",
            "I'm a good communicator",
            "Technical Knowledge", "Time Management",
            "Problem-Solving", "Patience", "Positivity", "Empathy",
        ];

        for ($i = 0; $i < count($skills); $i++) {
            Skill::create(
                [
                    'name' => $skills[$i]
                ]
            );
        }
    }
}
