<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
            "Maths",
            "English",
            "Basic Science",
            "Technical Drawing",
            "Literature in English",
            "Phonics",
            "Physics",
            "Chemistry",
            "Further Mathematics",
            "Economics",
            "French",
            "Yoruba",
            "PHE",
            "Numeracy",
            "Fine Arts"
        ];

        for ($i = 0; $i < count($subjects); $i++) {
            Subject::create(
                [
                    'name' => $subjects[$i]
                ]
            );
        }
    }
}
