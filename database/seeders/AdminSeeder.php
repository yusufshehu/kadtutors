<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user = User::factory()->create(
            [
                'email' => 'john2@doe.com',
            ]
        );
        Admin::factory()->create(
            [
                'user_id' => $user->id,
            ]
        );
        $user->assignRole('Admin');
    }
}
