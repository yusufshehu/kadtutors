<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tutor_jobs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tutor_id')->references('id')->on('tutors');
            $table->string('job_title');
            $table->string('employer');
            $table->string('employment_duration');
            $table->boolean('is_current_job');
            // $table->string('employed_from');
            // $table->string('employed_to');
            $table->string('job_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutor_jobs');
    }
}
