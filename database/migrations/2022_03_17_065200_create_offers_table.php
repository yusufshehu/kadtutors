<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tutor_id')->references('id')->on('tutors');
            $table->foreignId('offerer_user_id')->references('id')->on('users');
            $table->integer('status');
            $table->string('agreement_duration');
            $table->string('proposed_price');
            $table->string('price_paid')->nullable();
            $table->string('starting_date');
            $table->text('further_description')->nullable();
            $table->integer('number_of_students');
            $table->integer('number_of_sessions_a_week');
            $table->string('time_of_day');
            $table->string('duration_per_session');
            $table->json('subjects');
            $table->json('session_days');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
