<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeChangesToOffers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropConstrainedForeignId('offerer_user_id');
            $table->dropColumn('offerer_user_id', 'time_of_day');
            $table->foreignId('client_id')->references('id')->on('clients');
            $table->string('agreement_duration')->nullable()->change();
            $table->string('starting_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->foreignId('offerer_user_id')->references('id')->on('users');
            $table->string('time_of_day');
            $table->dropColumn('client_id');
            $table->string('agreement_duration');
        });
    }
}
