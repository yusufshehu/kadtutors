<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('client_id')->references('id')->on('clients');
            $table->string('job_duration');
            $table->decimal('proposed_price');
            $table->integer('number_of_students');
            $table->integer('number_of_sessions_a_week');
            $table->string('minimum_years_of_experience');
            $table->text('job_description');
            $table->text('how_to_apply');
            $table->integer('payment_interval');
            $table->json('subjects');
            $table->json('session_days');
            $table->string('time_of_day')->nullable();
            $table->string('duration_per_session')->nullable();
            $table->string('starting_date')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_posts');
    }
}
