<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTutorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'tutors',
            function (Blueprint $table) {
                $table->id();
                // $table->string('first_name');
                // $table->string('middle_name')->nullable();
                // $table->string('last_name');
                $table->foreignId('user_id')->nullable()->references('id')->on('users');
                $table->string('profile_picture_url')->nullable();
                // $table->string('email')->unique();
                // $table->string('phone_number')->unique();
                $table->text('bio')->nullable();
                $table->string('title')->nullable();
                $table->string('state_of_residence');
                $table->string('lga_of_residence');
                $table->text('address')->nullable();
                // $table->json('days_available');
                // $table->boolean('approved')->default(0);
                $table->integer('status')->default(1);
                // $table->text('work-experience');
                $table->timestamp('email_verified_at')->nullable();
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutors');
    }
}
